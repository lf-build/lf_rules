﻿using System.Collections.Generic;
using System.Web;

namespace Phoenix.Common
{
    public class SalesGroupsNavigation
    {
        public string AccountId { get; set; }

        public int DealId { get; set; }
    }

    /// <summary>
    /// Class for storing UserInformation in session object
    /// </summary>
    public class SessionUserModel
    {
        public bool IsManager { get; set; }

        public string Name { get; set; }

        public string Password { get; set; }

        public int[] PermissionId { get; set; }

        public int PrimaryRoleId { get; set; }

        public IList<int> RoleIds { get; set; }

        public IList<string> RoleNames { get; set; }

        public List<SalesGroupsNavigation> SalesGroupsNavigation { get; set; }

        public int UserId { get; set; }

        public string Username { get; set; }
    }

    public class StateManagement
    {
        /// <summary>
        /// Will store Value in session
        /// </summary>
        /// <param name="key">Key of session</param>
        /// <param name="value">Value of session</param>
        /// <returns></returns>
        public static string AddSession(string key, string value)
        {
            HttpContext.Current.Session.Add(key, value);
            return HttpContext.Current.Session[key].ToString();
        }

        /// <summary>
        /// Add SessionUserModel into session
        /// </summary>
        /// <param name="user">User Model for Session</param>
        /// <returns></returns>
        public static void AddUserSession(SessionUserModel user)
        {
            if (user != null)
            {
                HttpContext.Current.Session.Add("user", user);
            }
        }

        /// <summary>
        /// Retrive session by key
        /// </summary>
        /// <param name="key">Key of session</param>
        /// <returns>Session value</returns>
        public static string GetSession(string key)
        {
            return HttpContext.Current.Session[key].ToString();
        }

        /// <summary>
        /// Get the user of the current session.
        /// </summary>
        /// <returns>SessionUserModel</returns>
        public static SessionUserModel GetUserSession()
        {
            if (HttpContext.Current != null && HttpContext.Current.Session != null)
            {
                var user = HttpContext.Current.Session["user"];
                var sessionUser = user as SessionUserModel;
                if (sessionUser != null)
                {
                    return sessionUser;
                }
            }

            return null;
        }

        /// <summary>
        /// Remove session by key.
        /// </summary>
        /// <param name="key">Key of session</param>
        public static void RemoveSession(string key)
        {
            HttpContext.Current.Session.Remove(key);
        }

        /// <summary>
        /// Remove User Session .
        /// </summary>
        public static void RemoveUserSession()
        {
            HttpContext.Current.Session.Abandon();

            //HttpContext.Current.Session.Remove("user");
            //HttpContext.Current.Session.Remove("PermisionId");
            //HttpContext.Current.Session.Remove("PrimaryRoleId");
        }

        public static SessionUserModel SetPermissions(int[] permissions)
        {
            SessionUserModel usermodel = GetUserSession();
            if (usermodel != null)
            {
                usermodel.PermissionId = permissions;
            }

            return usermodel;
        }

        public static SessionUserModel SetPrimaryRoleID(int roleId)
        {
            SessionUserModel usermodel = GetUserSession();
            if (usermodel != null)
            {
                usermodel.PrimaryRoleId = roleId;
            }

            return usermodel;
        }
    }
}