﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.DirectoryServices;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;

namespace Sigma.LendFoundry.Common
{
    public static class Extension
    {
        public static string ConvertStringToDate(this string source)
        {
            if (source != null)
            {
                var dtime = Convert.ToDateTime(source);
                return dtime.ToString("MM/dd/yyyy hh:mm:ss tt");
            }
            else
                return null;
        }

        public static T ConvertTo<T>(this object o)
        {
            if (o == null || o == DBNull.Value)
            {
                return default(T);
            }

            Type type = typeof(T);
            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                if (type.GenericTypeArguments != null && type.GenericTypeArguments.Length == 1)
                {
                    type = type.GenericTypeArguments[0];
                }
                else
                {
                    return default(T);
                }
            }

            return (T)Convert.ChangeType(o, type);
        }

        public static IEnumerable<TSource> DistinctBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            HashSet<TKey> seenKeys = new HashSet<TKey>();
            foreach (TSource element in source)
            {
                if (seenKeys.Add(keySelector(element)))
                {
                    yield return element;
                }
            }
        }

        /// <summary>
        /// Escape a table name properly.
        /// </summary>
        /// <param name="sqlString"></param>
        /// <returns></returns>
        public static string EscapeSqlName(this string name)
        {
            return "[" + name.Replace("]", "]]") + "]";
        }

        /// <summary>
        /// Escape a string to be inserted into SQL as a SQL string to prevent SQL injection.
        /// </summary>
        /// <param name="sqlString"></param>
        /// <returns></returns>
        public static string EscapeSqlString(this string sqlString)
        {
            return sqlString.Replace("'", "''");
        }

        public static string GetAssemblyFolder(this Assembly assembly)
        {
            if (assembly == null) { throw new ArgumentNullException("assembly"); }

            string codeBase = assembly.CodeBase;
            UriBuilder uri = new UriBuilder(codeBase);
            string path = Uri.UnescapeDataString(uri.Path);
            return Path.GetDirectoryName(path);
        }

        public static object GetDefaultValue(this Type t)
        {
            if (t == null) { return null; }
            if (t.IsClass) { return null; }
            if (t.HasElementType) { return null; }

            TypeCode typeCode = Type.GetTypeCode(t);
            switch (typeCode)
            {
                case TypeCode.Boolean:
                    return false;

                case TypeCode.String:
                case TypeCode.Object:
                case TypeCode.Empty:
                case TypeCode.DBNull:
                    return null;

                case TypeCode.DateTime:
                    return DateTime.MinValue;

                case TypeCode.Char:
                    return Char.MinValue;

                default:
                    return 0;
            }
        }

        public static string GetExtension(this string source)
        {
          //  if (source == null) 
            //** 1092 ticket change
            if (string.IsNullOrWhiteSpace(source)) 
            { return null; }

            return Path.GetExtension(source);
        }

        /// <summary>
        /// Get all extension methods for the extendedType from the given assembly.
        /// </summary>
        /// <param name="assembly"></param>
        /// <param name="extendedType"></param>
        /// <returns></returns>
        public static IEnumerable<MethodInfo> GetExtensionMethods(this Assembly assembly, Type extendedType)
        {
            if (assembly == null) { throw new ArgumentNullException("assembly"); }
            if (extendedType == null) { throw new ArgumentNullException("extendedType"); }

            List<MethodInfo> methodInfos = new List<MethodInfo>();
            foreach (Type type in assembly.GetTypes().Where(t => t.IsSealed && !t.IsGenericType && !t.IsNested))
            {
                foreach (MethodInfo methodInfo in type.GetMethods(BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic).Where(mi => mi.IsDefined(typeof(ExtensionAttribute), false)))
                {
                    Type firstParamType = methodInfo.GetParameters()[0].ParameterType;
                    if (IsSubclassOrImplements(extendedType, firstParamType))
                    {
                        methodInfos.Add(methodInfo);
                    }
                }
            }

            return methodInfos;
        }

        public static string GetFederalTaxIdFormat(this string source)
        {
            if (string.IsNullOrWhiteSpace(source))
            {
                return string.Empty;
            }

            try
            {
                long convertedNumber = Convert.ToInt64(source.Replace(" ", string.Empty).Replace("-", string.Empty).Replace("(", string.Empty).Replace(")", string.Empty));
                return String.Format("{0:##-#######}", convertedNumber);
            }
            catch (FormatException)
            {
                // Incorrectly typed phone number
                return source;
            }
        }

        public static string GetLandlineFormat(this string source)
        {
            if (string.IsNullOrWhiteSpace(source))
            {
                return string.Empty;
            }

            try
            {
                long convertedNumber = Convert.ToInt64(source.Replace(" ", string.Empty).Replace("-", string.Empty).Replace("(", string.Empty).Replace(")", string.Empty));
                return String.Format("{0:###-###-####}", convertedNumber);
            }
            catch (FormatException)
            {
                // Incorrectly typed phone number
                return source;
            }
        }

        public static string GetPhoneFormat(this string source)
        {
            if (string.IsNullOrWhiteSpace(source))
            {
                return string.Empty;
            }

            try
            {
                long convertedNumber = Convert.ToInt64(source.Replace(" ", string.Empty).Replace("-", string.Empty).Replace("(", string.Empty).Replace(")", string.Empty));
                return String.Format("{0:(###) ###-####}", convertedNumber);
            }
            catch (FormatException)
            {
                // Incorrectly typed phone number
                return source;
            }
        }

        public static string GetRandonFileName()
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            var result = new string(
                Enumerable.Repeat(chars, 15)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());
            return result;
        }

        public static List<Dictionary<string, object>> GetRows(this SqlCommand command)
        {
            var rows = new List<Dictionary<string, object>>();
            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    rows.Add(GetSingleRow(reader));
                }
            }

            return rows;
        }

        public static Dictionary<string, object> GetSingleRow(this SqlDataReader reader)
        {
            Dictionary<string, object> row = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
            for (int i = 0; i < reader.FieldCount; i++)
            {
                string columnName = reader.GetName(i);
                object o = reader[i];
                row[columnName] = o == DBNull.Value ? null : o;
            }

            return row;
        }

        public static string GetSsnFormat(this string source)
        {
            if (string.IsNullOrWhiteSpace(source))
            {
                return string.Empty;
            }

            try
            {
                long convertedNumber = Convert.ToInt64(source.Replace(" ", string.Empty).Replace("-", string.Empty).Replace("(", string.Empty).Replace(")", string.Empty));
                return String.Format("{0:###-##-####}", convertedNumber);
            }
            catch (FormatException)
            {
                // Incorrectly typed phone number
                return source;
            }
        }

        public static string GetValidFileName(this string source)
        {
         //   if (source == null)
            //** 1092 ticket change
                if (string.IsNullOrWhiteSpace(source )) 
                return "Unknown";

            source = Regex.Replace(source, @"\s", string.Empty);

            //System.IO.Path.GetInvalidFileNameChars()
            foreach (char c in System.IO.Path.GetInvalidFileNameChars())
            {
                source = source.Replace(c, '-');
            }

            return source;
        }

        public static string GetValidSearchString(this string source)
        {
            if (string.IsNullOrEmpty(source)) { return source; }
            source = source.TrimEnd('.');
            source = source.TrimEnd(' ');

            string[] keywords = { "company:", "DBA:", "address:", "contact:", "owner:" };
            if (!source.Contains(":"))
            {
                //if (!source.Contains("&"))
                //{
                //    source = "text:(" + source + "*)";
                //}
                //else
                //{
                    source = "text:(" + source + ")";
                //}
            }
            else
            {
                source = source + "*";
            }

            return source;
        }

        public static string GetWildSearchString(this string source)
        {
            if (string.IsNullOrEmpty(source)) { return source; }
            source = source.TrimEnd('.');
            source = Regex.Replace(source, @"&\s+", "&");
            //source = source.Replace("&", "\"&\"");
            source = source.Replace("-", "\"-\"");
            //source = source.Replace(" ", "%");
            source = source.Replace("@", "\"@\"");

            string[] keywords = { "company:", "DBA:", "address:", "contact:", "owner:" };
            if (!source.Contains(":"))
            {
                if (!source.Contains("&"))
                {
                    source = "text:(" + source + "*)";
                }
                else
                {
                    source = "text:(" + source + ")";
                }
            }
            else
            {
                source = source + "*";
            }

            return source;
        }

        public static T GetValue<T>(this Dictionary<string, object> row, string key)
        {
            object v;
            if (row.TryGetValue(key, out v))
            {
                if (v != DBNull.Value && v != null)
                {
                    if (v is T)
                    {
                        return (T)v;
                    }

                    try
                    {
                        Type type = typeof(T);
                        if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                        {
                            type = type.GenericTypeArguments.FirstOrDefault() ?? type;
                        }

                        return (T)Convert.ChangeType(v, type);
                    }
                    catch
                    {
                    }
                }
            }

            return default(T);
        }

        /// <summary>
        /// Get the dayOfWeek before or after the given date.
        /// </summary>
        /// <param name="fromDate"></param>
        /// <param name="dayOfWeek"></param>
        /// <returns></returns>
        public static DateTime GetWeekday(this DateTime fromDate, DayOfWeek dayOfWeek, bool before)
        {
            if (before)
            {
                if (dayOfWeek < fromDate.DayOfWeek)
                {
                    return fromDate.Subtract(TimeSpan.FromDays(fromDate.DayOfWeek - dayOfWeek));
                }
                else if (dayOfWeek > fromDate.DayOfWeek)
                {
                    return fromDate.AddDays(dayOfWeek - fromDate.DayOfWeek).Subtract(TimeSpan.FromDays(7));
                }
            }
            else // after
            {
                if (dayOfWeek < fromDate.DayOfWeek)
                {
                    return fromDate.Subtract(TimeSpan.FromDays(fromDate.DayOfWeek - dayOfWeek)).AddDays(7);
                }
                else if (dayOfWeek > fromDate.DayOfWeek)
                {
                    return fromDate.AddDays(dayOfWeek - fromDate.DayOfWeek);
                }
            }

            return fromDate;
        }

        /// <summary>
        /// Get the dayOfWeek closest to the given date.
        /// </summary>
        /// <param name="fromDate"></param>
        /// <param name="dayOfWeek"></param>
        /// <returns></returns>
        public static DateTime GetWeekdayClosest(this DateTime fromDate, DayOfWeek dayOfWeek)
        {
            if (dayOfWeek > fromDate.DayOfWeek)
            {
                return fromDate.AddDays(dayOfWeek - fromDate.DayOfWeek);
            }
            else if (dayOfWeek < fromDate.DayOfWeek)
            {
                return fromDate.Subtract(TimeSpan.FromDays(fromDate.DayOfWeek - dayOfWeek));
            }

            return fromDate;
        }

        /// <summary>
        /// Return whether type is a subclass of targetType or implements the parentType (if the
        /// parentType is an interface).
        /// </summary>
        /// <param name="type"></param>
        /// <param name="parentType"></param>
        /// <returns></returns>
        public static bool IsSubclassOrImplements(this Type type, Type parentType)
        {
            return type.IsSubclassOf(parentType) || type.GetInterface(parentType.Name) != null;
        }

        public static string PhoneFormat(this string source)
        {
            if (string.IsNullOrWhiteSpace(source))
            {
                return string.Empty;
            }

            return string.Concat(source.Where(i => !new[] { ' ', '-', '(', ')' }.Contains(i)));
        }

        /// <summary>
        /// Get the content of the given stream
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static byte[] ToByteArray(this Stream stream)
        {
            // If no stream, return empty array.
            if (stream == null) { return new byte[0]; }

            // If already a MemoryStream, return the content.
            MemoryStream ms = stream as MemoryStream;
            if (ms != null) { return ms.ToArray(); }

            // Copy the content to a MemoryStream then return
            byte[] buffer = new byte[16 * 1024];
            using (ms = new MemoryStream())
            {
                int read;
                while ((read = stream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }

                return ms.ToArray();
            }
        }

        /// <summary>
        /// Get the Xml doc as bytes.
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <returns></returns>
        public static byte[] ToBytes(this XmlDocument xmlDoc)
        {
            using (MemoryStream outStream = new MemoryStream())
            {
                using (XmlWriter xmlWriter = XmlWriter.Create(outStream,
                    new XmlWriterSettings
                    {
                        CloseOutput = true,
                        Indent = true,
                        WriteEndDocumentOnClose = true,
                        Encoding = Encoding.UTF8
                    }))
                {
                    xmlDoc.Save(xmlWriter);
                }

                return outStream.ToArray();
            }
        }
    }

    public class Utility
    {
        /// <summary>
        /// deserialize xml column
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static T Deserialize<T>(string xml)
        {
            if (string.IsNullOrWhiteSpace(xml))
            {
                return default(T);
            }

            XmlSerializer serializer = new XmlSerializer(typeof(T));
            XmlReaderSettings settings = new XmlReaderSettings
            {
                DtdProcessing = DtdProcessing.Prohibit
            };

            using (StringReader textReader = new StringReader(xml))
            {
                using (XmlReader xmlReader = XmlReader.Create(textReader, settings))
                {
                    return (T)serializer.Deserialize(xmlReader);
                }
            }
        }

        /// <summary>
        /// Check whether the given user is in the given usergroup.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="adPath"></param>
        /// <param name="groupName"></param>
        /// <returns></returns>
        public static bool IsUserInGroup(string username, string password, string adPath, string groupName)
        {
            using (var entry = new DirectoryEntry(adPath, username, password))
            {
                return IsUserInGroup(entry, username, groupName);
            }
        }

        /// <summary>
        /// Check whether the given user is in the given usergroup.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="adPath"></param>
        /// <param name="groupName"></param>
        /// <returns></returns>
        public static bool IsUserInGroup(DirectoryEntry dirEntry, string username, string groupName)
        {
            using (var search = new DirectorySearcher(dirEntry))
            {
                search.Filter = "(&(objectClass=user)(|(cn=" + username + ")(sAMAccountName=" + username + ")))";
                search.PropertiesToLoad.Add("memberOf");
                try
                {
                    SearchResult result = search.FindOne();
                    if (result == null)
                    {
                        return false;
                    }

                    int groupCount = result.Properties["memberOf"].Count;

                    bool isInProperGroup = false;
                    for (int counter = 0; counter < groupCount; counter++)
                    {
                        string curGroupName = (string)result.Properties["memberOf"][counter];
                        if (string.Equals(groupName, curGroupName, StringComparison.OrdinalIgnoreCase))
                        {
                            isInProperGroup = true;
                            break;
                        }
                    }

                    return isInProperGroup;
                }
                catch (DirectoryServicesCOMException)
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Serialize xml column
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string Serialize<T>(T value)
        {
            if (value == null)
            {
                return null;
            }

            XmlSerializer serializer = new XmlSerializer(typeof(T));
            XmlWriterSettings settings = new XmlWriterSettings()
            {
                Encoding = new UnicodeEncoding(false, false),
                Indent = false,
                OmitXmlDeclaration = true
            };

            using (StringWriter textWriter = new StringWriter())
            {
                using (XmlWriter xmlWriter = XmlWriter.Create(textWriter, settings))
                {
                    serializer.Serialize(xmlWriter, value);
                }

                return textWriter.ToString();
            }
        }
    }
}