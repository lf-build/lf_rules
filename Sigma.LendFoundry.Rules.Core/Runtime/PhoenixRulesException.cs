﻿using System;

namespace Sigma.LendFoundry.Rules.Core.Runtime
{
    public class PhoenixRulesException : Exception
    {
        public PhoenixRulesException(string message)
            : base(message)
        {
        }

        public PhoenixRulesException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}