﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Text.RegularExpressions;
using Phoenix.Common;

namespace Sigma.LendFoundry.Rules.Core.Runtime
{
    public static class RulesUtils
    {
        public const string DataReferenceBegin = "$[";
        public const char DataReferenceEnd = ']';
        public const char VariableBegin = '[';
        public const char VariableEnd = ']';

        public static string ExpandRuleExpression(this string ruleExpression, Dictionary<string, string> variables, out List<string> dataRefs)
        {
            dataRefs = new List<string>();

            if (string.IsNullOrWhiteSpace(ruleExpression)) { return ruleExpression; }

            ruleExpression = ruleExpression.ExpandVariables(variables);
            dataRefs.AddRange(ruleExpression.ExtractDataReferences());

            return ruleExpression;
        }

        public static List<string> ExtractDataReferences(this string ruleExpression)
        {
            if (string.IsNullOrWhiteSpace(ruleExpression)) { return new List<string>(); }

            return ruleExpression.Extract(DataReferenceBegin, DataReferenceEnd.ToString());
        }

        public static List<string> ExtractVariables(this string ruleExpression)
        {
            if (string.IsNullOrWhiteSpace(ruleExpression)) { return new List<string>(); }

            return ruleExpression.Extract(VariableBegin.ToString(), VariableEnd.ToString());
        }

        public static Dictionary<string, string> GetAllVariables(SqlConnection conn, DateTime dealDate)
        {
            if (conn == null) { throw new ArgumentNullException("conn"); }
            if (conn.State != ConnectionState.Open) { throw new ArgumentException("conn", "Connection is not opened."); }

            string script = @"SELECT [VariableName], [ValueExpression]
  FROM [dbo].[VariableList]
  inner join [dbo].[VariableVersion]
  on [dbo].[VariableList].VariableID = [dbo].[VariableVersion].VariableID
  order by [VersionNumber] desc";

            Dictionary<string, string> variables = new Dictionary<string, string>();
            using (SqlCommand com = conn.CreateCommand())
            {
                com.CommandText = script;
                using (SqlDataReader reader = com.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        variables.Add(reader["VariableName"].ToString(), reader["ValueExpression"].ToString());
                    }
                }
            }

            return variables;
        }

        public static DateTime GetDealDate(SqlConnection conn, string dealId)
        {
            using (SqlCommand com = conn.CreateCommand())
            {
                com.CommandText = string.Format(CultureInfo.InvariantCulture,
                    "select [CreatedOn] from [dbo].[Deal] where [DealId] = {0}",
                    dealId);
                object result = com.ExecuteScalar();
                if (result is DateTime)
                {
                    return (DateTime)result;
                }

                return DateTime.Now;
            }
        }

        /// <summary>
        /// Strip the [ and ] from the string
        /// </summary>
        /// <param name="variable"></param>
        /// <returns></returns>
        public static string GetRawVariableName(this string variable)
        {
            if (string.IsNullOrWhiteSpace(variable)) { return variable; }

            return variable.Trim(VariableBegin, VariableEnd);
        }

        /// <summary>
        /// Normalize a rule-expression according to the given Context.
        /// </summary>
        /// <param name="ruleExpression"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static string NormalizeRule(this string ruleExpression, RuleEvaluationContext context)
        {
            try
            {
                var dataRefs = ruleExpression.Extract(DataReferenceBegin, DataReferenceEnd.ToString());

                foreach (string dataRef in dataRefs)
                {
                    dynamic value = context[dataRef];
                    string typeName = value != null ? value.GetType().Name : "object";

                    var replaceString = string.Empty;
                    replaceString = string.Format(CultureInfo.InvariantCulture, "(({0})context[\"{1}\"])", typeName, dataRef.Replace("\\", "\\\\"));
                    ruleExpression = ruleExpression.Replace(dataRef, replaceString);
                }

                ruleExpression = ruleExpression.Replace("CheckRegex", "context.CheckRegex");
                ruleExpression = ruleExpression.Replace("HasValue", "context.HasValue");
                ruleExpression = ruleExpression.Replace("GetCount", "context.GetCount");

                ruleExpression = string.Format("({0} context)=> {1}", context.GetType().Name, ruleExpression);
            }
            catch (PhoenixRulesException ex)
            {
                LoggerFactory.LoggerInstance.LogInformation(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                LoggerFactory.LoggerInstance.LogFatal(ex);
                throw;
            }

            return ruleExpression;
        }

        private static string ExpandVariables(this string ruleExpression, Dictionary<string, string> variables)
        {
            List<string> ruleVars = ruleExpression.Extract(VariableBegin.ToString(), VariableEnd.ToString());
            foreach (string variable in ruleVars)
            {
                string variableName = variable.Trim(VariableBegin, VariableEnd);
                string varExpression = null;
                if (variables.TryGetValue(variableName, out varExpression))
                {
                    string variableExpr = varExpression;
                    ruleExpression = ruleExpression.Replace(variable, variableExpr);
                }
            }

            return ruleExpression;
        }

        private static List<string> Extract(this string source, string start, string end)
        {
            var results = new HashSet<string>();
            string pattern = string.Format(
                "{0}{1}{2}",
                Regex.Escape(start),
                ".*?",
                Regex.Escape(end));

            string empty = string.Format(
                "{0}{1}",
                Regex.Escape(start),
                Regex.Escape(end));

            foreach (var match in Regex.Matches(source, pattern))
            {
                string result = match.ToString();
                if (!string.Equals(empty, result))
                {
                    results.Add(result);
                }
            }

            return new List<string>(results);
        }
    }
}