﻿// Copyright © Quick Bridge Funding 2014
// 
// Dev: Nghi Nguyen (nnguyen)
// Test: Nghi Nguyen (nnguyen)

using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Sigma.LendFoundry.Rules.Core.Runtime
{
    internal class JumpInfo
    {
        public LabelExpression breakLabel;
        public LabelExpression continueLabel;
    }

    internal class ParseInfo
    {
        public readonly Stack<JumpInfo> jumpInfoStack = new Stack<JumpInfo>();
        public readonly Stack<Dictionary<string, ParameterExpression>> localVariableStack = new Stack<Dictionary<string, ParameterExpression>>();
        public readonly List<ParameterExpression> referredParameterList = new List<ParameterExpression>();
        public LabelExpression returnLabel = null;

        public Expression GetReferredParameter(Type type, string name)
        {
            Expression exp = null;
            // Parameter reference
            foreach (ParameterExpression pe in referredParameterList)
            {
                if (pe.Type == type && string.Equals(pe.Name, name))
                {
                    exp = pe;
                    break;
                }
            }

            if (exp == null)
            {
                exp = Expression.Parameter(type, name);
                referredParameterList.Add((ParameterExpression)exp);
            }

            return exp;
        }

        // local variables in every statement block
        public Expression GetReferredVariable(string varName)
        {
            foreach (Dictionary<string, ParameterExpression> dic in localVariableStack)
            {
                ParameterExpression paramExpr;
                if (dic.TryGetValue(varName, out paramExpr))
                {
                    return paramExpr;
                }
            }

            return null;
        }
    }
}