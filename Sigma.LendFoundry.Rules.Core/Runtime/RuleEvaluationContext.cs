﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace Sigma.LendFoundry.Rules.Core.Runtime
{
    public class RuleEvaluationContext
    {
        protected readonly Dictionary<string, DataReferenceVariable> variables = new Dictionary<string, DataReferenceVariable>();

        public RuleEvaluationContext(IEnumerable<DataReferenceVariable> dataRefVariables)
        {
            if (dataRefVariables != null)
            {
                foreach (DataReferenceVariable drv in dataRefVariables)
                {
                    this.variables.Add(drv.Name, drv);
                }
            }
        }

        public object this[string key]
        {
            get
            {
                DataReferenceVariable var;
                if (this.variables.TryGetValue(key, out var))
                {
                    return var.Result;
                }

                throw new PhoenixRulesException(string.Format(CultureInfo.CurrentCulture, "Data Reference '{0}' is not valid.", key));
            }
        }

        //public List<Test> tests = new List<Test>();
        /// <summary>
        /// Check Regex
        /// </summary>
        /// <param name="input"></param>
        /// <param name="pattern"></param>
        /// <returns></returns>
        public bool CheckRegex(string input, string pattern)
        {
            bool result = false;
            pattern = pattern.Replace("\\\\", "\\");
            pattern = pattern.Replace("\\", @"\");
            if (Regex.IsMatch(input, pattern))
            {
                result = true;
            }
            return result;
        }

        /// <summary>
        /// Check Valid Values using Comma separated
        /// </summary>
        /// <param name="input"></param>
        /// <param name="validValues"></param>
        /// <returns></returns>
        public bool HasValue(object input, string validValues)
        {
            bool result = false;
            if (!string.IsNullOrEmpty(validValues))
            {
                string inputStr = input != null ? input.ToString() : "null";
                string[] lstValidValues = validValues.Split(',');
                if (lstValidValues.Any(item => string.Equals(item.Trim(), inputStr, StringComparison.Ordinal)))
                {
                    result = true;
                }
            }

            return result;
        }

        /// <summary>
        /// Check Valid Values using Comma separated
        /// </summary>
        /// <param name="input"></param>
        /// <param name="validValues"></param>
        /// <returns></returns>
        public bool HasValue(int input, string validValues)
        {
            bool result = false;
            if (!string.IsNullOrEmpty(validValues))
            {
                string inputStr = input.ToString();
                string[] lstValidValues = validValues.Split(',');
                if (lstValidValues.Any(item => string.Equals(item.Trim(), inputStr, StringComparison.Ordinal)))
                {
                    result = true;
                }
            }

            return result;
        }

        /// <summary>
        /// Check Valid Values using Comma separated
        /// </summary>
        /// <param name="input"></param>
        /// <param name="validValues"></param>
        /// <returns></returns>
        public bool HasValue(double input, string validValues)
        {
            bool result = false;
            if (!string.IsNullOrEmpty(validValues))
            {
                string inputStr = input.ToString();
                string[] lstValidValues = validValues.Split(',');
                if (lstValidValues.Any(item => string.Equals(item.Trim(), inputStr, StringComparison.Ordinal)))
                {
                    result = true;
                }
            }

            return result;
        }
    }
}