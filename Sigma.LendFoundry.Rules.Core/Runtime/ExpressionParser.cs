﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using Sigma.LendFoundry.Common;

namespace Sigma.LendFoundry.Rules.Core.Runtime
{
    #region Enums

    public enum OperatorType
    {
        UNKNOWN,
        OPEN,   // (,[,{
        CLOSE,  // ),],}
        PREFIX_UNARY,  // +,-,++,--
        POST_UNARY, // ++, --
        BINARY,  // +,-,*,/
        CONDITIONAL,    // (c)?x:y
        ASSIGN, // =, +=
        PRIMARY   // , ;
    }

    internal enum RequiredOperandType { NONE, SAME }

    internal enum TokenType { NONE, COMMENT, COMMENT_BLOCK, TEXT, INT, UINT, LONG, ULONG, FLOAT, DOUBLE, DECIMAL, BOOL, IDENTIFIER, OPERATOR, CHAR }

    #endregion Enums

    #region Delegates

    internal delegate Expression d1m(Expression exp, MethodInfo mi);

    internal delegate Expression d2(Expression exp1, Expression exp2);

    internal delegate Expression d2bm(Expression exp1, Expression exp2, bool liftToNull, MethodInfo methodInfo);

    internal delegate Expression d2m(Expression exp1, Expression exp2, MethodInfo methodInfo);

    #endregion Delegates

    /// <summary>
    /// ExpressionParser has 3 naming scope Global : available for all Parser instance. Parser :
    /// available for current Parser instance. Inline : available for current expression.
    /// 
    /// User can create multiple parser instance for different naming scope One parser can create
    /// multiple expression with the same naming scope One expression can define in-line namespace
    /// and parameter only for current expression scope
    /// </summary>
    public class ExpressionParser
    {
        #region namespace, parameter and type: case sensitive

        // parameter type
        private static readonly Dictionary<string, Type> GlobalParameterType = new Dictionary<string, Type>();

        // parameter Value
        private static readonly Dictionary<string, object> GlobalParameterValue = new Dictionary<string, object>();

        // implicit conversion
        private static Dictionary<Type, List<Type>> ImplicitConversions = CreateImplicitConversionTypeMap();

        private static Dictionary<Type, Type> NullableTypes = CreateNullableTypeMap();

        private static Dictionary<string, Type> SystemTypes = CreateSystemTypeMap();

        // Global scope
        private readonly Dictionary<string, Type> _parameterType = new Dictionary<string, Type>();

        // Global scope
        private readonly Dictionary<string, object> _parameterValue = new Dictionary<string, object>();

        // namespace for static data type
        private readonly Using usingInstance = new Using();

        private Assembly _callingAssembly = null;

        // Parser scope
        private Dictionary<string, Type> _inlineParameterType = null;

        public ExpressionParser(params Tuple<string, string>[] usings)
        {
            // Default namespace
            this.usingInstance.Add("System.Text.RegularExpressions", "System");
            this.usingInstance.Add("System.Collection", "System"); // add namespace for default assembly mscorlib
            this.usingInstance.Add("System.Linq.Expressions", "System.Core"); // add namespace with assembly if other than mscorlib
            this.usingInstance.Add("System.Linq", "System.Core"); // add namespace with assembly if other than mscorlib

            if (usings != null)
            {
                foreach (Tuple<string, string> us in usings)
                {
                    AddUsing(us.Item1, us.Item2);
                }
            }
        }

        public void AddUsing(string namespaceName, string assembly)
        {
            this.usingInstance.Add(namespaceName, assembly);
        }

        public void AddUsingContext(RuleEvaluationContext context)
        {
            AddUsing(context.GetType().Namespace, context.GetType().Assembly.FullName);
        }

        internal Type QueryImplicitConversionType(Expression ex1, Expression ex2)
        {
            List<Type> ex1ConvertibleTypes;
            List<Type> ex2ConvertibleTypes;

            if (ImplicitConversions.TryGetValue(ex1.Type, out ex1ConvertibleTypes) &&
                ImplicitConversions.TryGetValue(ex2.Type, out ex2ConvertibleTypes))
            {
                foreach (Type t in ex1ConvertibleTypes)
                {
                    if (ex2ConvertibleTypes.Contains(t))
                    {
                        return t;
                    }
                }
            }

            if (ex1 is ConstantExpression && ((ConstantExpression)ex1).Value == null && !ex2.Type.IsValueType) return ex2.Type;
            if (ex2 is ConstantExpression && ((ConstantExpression)ex2).Value == null && !ex1.Type.IsValueType) return ex1.Type;

            return null;
        }

        /// <summary>
        /// check by order Parameter Type Globel Parameter Type queryParameterValue(paramName)
        /// </summary>
        /// <param name="paramName"></param>
        /// <returns></returns>
        internal Type QueryParameterType(string paramName)
        {
            Type paramType;
            if (_parameterType.TryGetValue(paramName, out paramType) ||
                GlobalParameterType.TryGetValue(paramName, out paramType) ||
                _inlineParameterType.TryGetValue(paramName, out paramType))
            {
                return paramType;
            }

            return null;
        }

        /// <summary>
        /// check by order:
        /// - System Type - user type cannot be the same with system type. check system type first
        ///   is for performance
        /// - Type.GetType(typeName)
        /// - Type from Local Namespace
        /// - Type from Global Namespace limitation: Type.GetType use AssmblyQulifiedName: "ns.type,
        /// ns". not include version info, "type" part not include "."
        /// </summary>
        /// <param name="name">type or instance object name</param>
        /// <returns></returns>
        internal Type QueryStaticType(string typeName)
        {
            Type staticType;
            if (SystemTypes.TryGetValue(typeName, out staticType))
            {
                return staticType;
            }

            staticType = Type.GetType(typeName);
            if (staticType != null)
            {
                return staticType;
            }

            if (_callingAssembly != null)
            {
                staticType = _callingAssembly.GetType(typeName);
                if (staticType != null)
                {
                    return staticType;
                }
            }

            // Check in list of namespaces
            foreach (string ns in this.usingInstance.NamespaceList.Keys)
            {
                string fullTypeName = ns + "." + typeName;
                Assembly assembly = null;
                if (!this.usingInstance.NamespaceList.TryGetValue(ns, out assembly) || assembly == null)
                {
                    staticType = Type.GetType(fullTypeName);
                    if (staticType != null)
                    {
                        return staticType;    // check System(mscorlib) assembly
                    }

                    if (_callingAssembly != null)
                    {
                        staticType = _callingAssembly.GetType(fullTypeName);
                        if (staticType != null)
                        {
                            return staticType;  // check caller assembly
                        }
                    }

                    staticType = Type.GetType(fullTypeName + ", " + ns);
                    if (staticType != null)
                    {
                        return staticType;   // check namespace as assembly name
                    }
                }
                else if ((staticType = assembly.GetType(fullTypeName)) != null)
                {
                    return staticType;
                }
            }

            if (typeName.Contains("."))
            {
                staticType = Type.GetType(typeName + ", " + typeName.Substring(0, typeName.LastIndexOf('.')));
                if (staticType != null)
                {
                    return staticType;
                }
            }

            //// check with Namespace
            //if (this.Namespace != null)
            //{
            //    foreach (string ns in this.Namespace)
            //        if ((type = Type.GetType(ns + "." + typeName)) != null
            //            || (type = Type.GetType(ns + "." + typeName + ", " + ns)) != null) return type;
            //}
            //// check with GlobalNamespace
            //if (GlobalNamespace != null)
            //{
            //    foreach (string ns in GlobalNamespace)
            //        if ((type = Type.GetType(ns + "." + typeName)) != null
            //            || (type = Type.GetType(ns + "." + typeName + ", " + ns)) != null) return type;
            //}
            //// check with localNamespace
            //if (inlineNamespace != null)
            //{
            //    foreach (string ns in inlineNamespace)
            //        if ((type = Type.GetType(ns + "." + typeName)) != null
            //            || (type = Type.GetType(ns + "." + typeName + ", " + ns)) != null) return type;
            //}
            return null;
        }

        private static Dictionary<Type, List<Type>> CreateImplicitConversionTypeMap()
        {
            Dictionary<Type, List<Type>> dic = new Dictionary<Type, List<Type>>();
            dic.Add(typeof(char), new List<Type>(new Type[] { typeof(char), typeof(ushort), typeof(int), typeof(uint), typeof(long), typeof(ulong), typeof(float), typeof(double), typeof(decimal), typeof(char?), typeof(ushort?), typeof(int?), typeof(uint?), typeof(long?), typeof(ulong?), typeof(float?), typeof(double?), typeof(decimal?) }));
            dic.Add(typeof(byte), new List<Type>(new Type[] { typeof(byte), typeof(short), typeof(ushort), typeof(int), typeof(uint), typeof(long), typeof(ulong), typeof(float), typeof(double), typeof(decimal), typeof(byte?), typeof(short?), typeof(ushort?), typeof(int?), typeof(uint?), typeof(long?), typeof(ulong?), typeof(float?), typeof(double?), typeof(decimal?) }));
            dic.Add(typeof(sbyte), new List<Type>(new Type[] { typeof(sbyte), typeof(short), typeof(int), typeof(long), typeof(float), typeof(double), typeof(decimal), typeof(sbyte?), typeof(short?), typeof(int?), typeof(long?), typeof(float?), typeof(double?), typeof(decimal?) }));
            dic.Add(typeof(short), new List<Type>(new Type[] { typeof(short), typeof(int), typeof(long), typeof(float), typeof(double), typeof(decimal), typeof(short?), typeof(int?), typeof(long?), typeof(float?), typeof(double?), typeof(decimal?) }));
            dic.Add(typeof(ushort), new List<Type>(new Type[] { typeof(ushort), typeof(int), typeof(uint), typeof(long), typeof(ulong), typeof(float), typeof(double), typeof(decimal), typeof(ushort?), typeof(int?), typeof(uint?), typeof(long?), typeof(ulong?), typeof(float?), typeof(double?), typeof(decimal?) }));
            dic.Add(typeof(int), new List<Type>(new Type[] { typeof(int), typeof(long), typeof(float), typeof(double), typeof(decimal), typeof(int?), typeof(long?), typeof(float?), typeof(double?), typeof(decimal?) }));
            dic.Add(typeof(uint), new List<Type>(new Type[] { typeof(uint), typeof(long), typeof(ulong), typeof(float), typeof(double), typeof(decimal), typeof(uint?), typeof(long?), typeof(ulong?), typeof(float?), typeof(double?), typeof(decimal?) }));
            dic.Add(typeof(long), new List<Type>(new Type[] { typeof(long), typeof(float), typeof(double), typeof(decimal), typeof(long?), typeof(float?), typeof(double?), typeof(decimal?) }));
            dic.Add(typeof(ulong), new List<Type>(new Type[] { typeof(ulong), typeof(float), typeof(double), typeof(decimal), typeof(ulong?), typeof(float?), typeof(double?), typeof(decimal?) }));
            dic.Add(typeof(float), new List<Type>(new Type[] { typeof(float), typeof(double), typeof(decimal), typeof(float?), typeof(double?), typeof(decimal?) }));
            dic.Add(typeof(double), new List<Type>(new Type[] { typeof(double), typeof(double?) }));
            dic.Add(typeof(decimal), new List<Type>(new Type[] { typeof(decimal), typeof(decimal?) }));

            dic.Add(typeof(char?), new List<Type>(new Type[] { typeof(char?), typeof(ushort?), typeof(int?), typeof(uint?), typeof(long?), typeof(ulong?), typeof(float?), typeof(double?), typeof(decimal?) }));
            dic.Add(typeof(byte?), new List<Type>(new Type[] { typeof(byte?), typeof(short?), typeof(ushort?), typeof(int?), typeof(uint?), typeof(long?), typeof(ulong?), typeof(float?), typeof(double?), typeof(decimal?) }));
            dic.Add(typeof(sbyte?), new List<Type>(new Type[] { typeof(sbyte?), typeof(short?), typeof(int?), typeof(long?), typeof(float?), typeof(double?), typeof(decimal?) }));
            dic.Add(typeof(short?), new List<Type>(new Type[] { typeof(short?), typeof(int?), typeof(long?), typeof(float?), typeof(double?), typeof(decimal?) }));
            dic.Add(typeof(ushort?), new List<Type>(new Type[] { typeof(ushort?), typeof(int?), typeof(uint?), typeof(long?), typeof(ulong?), typeof(float?), typeof(double?), typeof(decimal?) }));
            dic.Add(typeof(int?), new List<Type>(new Type[] { typeof(int?), typeof(long?), typeof(float?), typeof(double?), typeof(decimal?) }));
            dic.Add(typeof(uint?), new List<Type>(new Type[] { typeof(uint?), typeof(long?), typeof(ulong?), typeof(float?), typeof(double?), typeof(decimal?) }));
            dic.Add(typeof(long?), new List<Type>(new Type[] { typeof(long?), typeof(float?), typeof(double?), typeof(decimal?) }));
            dic.Add(typeof(ulong?), new List<Type>(new Type[] { typeof(ulong?), typeof(float?), typeof(double?), typeof(decimal?) }));
            dic.Add(typeof(float?), new List<Type>(new Type[] { typeof(float?), typeof(double?), typeof(decimal?) }));
            dic.Add(typeof(double?), new List<Type>(new Type[] { typeof(double?) }));
            dic.Add(typeof(decimal?), new List<Type>(new Type[] { typeof(decimal?) }));

            return dic;
        }

        private static Dictionary<Type, Type> CreateNullableTypeMap()
        {
            Dictionary<Type, Type> dic = new Dictionary<Type, Type>();
            dic.Add(typeof(bool), typeof(bool?));
            dic.Add(typeof(char), typeof(char?));
            dic.Add(typeof(byte), typeof(byte?));
            dic.Add(typeof(sbyte), typeof(sbyte?));
            dic.Add(typeof(short), typeof(short?));
            dic.Add(typeof(ushort), typeof(ushort?));
            dic.Add(typeof(int), typeof(int?));
            dic.Add(typeof(uint), typeof(uint?));
            dic.Add(typeof(long), typeof(long?));
            dic.Add(typeof(ulong), typeof(ulong?));
            dic.Add(typeof(float), typeof(float?));
            dic.Add(typeof(double), typeof(double?));
            dic.Add(typeof(decimal), typeof(decimal?));
            return dic;
        }

        private static Dictionary<string, Type> CreateSystemTypeMap()
        {
            // system Value types : http://msdn.microsoft.com/en-us/library/s1ax56ch.aspx
            Dictionary<string, Type> dic = new Dictionary<string, Type>();
            dic.Add("bool", typeof(bool));
            dic.Add("char", typeof(char));
            dic.Add("string", typeof(string));
            dic.Add("byte", typeof(byte));
            dic.Add("sbyte", typeof(sbyte));
            dic.Add("short", typeof(short));
            dic.Add("ushort", typeof(ushort));
            dic.Add("int", typeof(int));
            dic.Add("uint", typeof(uint));
            dic.Add("long", typeof(long));
            dic.Add("ulong", typeof(ulong));
            dic.Add("float", typeof(float));
            dic.Add("double", typeof(double));
            dic.Add("decimal", typeof(decimal));
            dic.Add("enum", typeof(Enum));
            dic.Add("object", typeof(object));
            return dic;
        }

        // Expr scope Parser scope

        #endregion namespace, parameter and type: case sensitive

        public LambdaExpression Parse(string expression)
        {
            try
            {
                _callingAssembly = Assembly.GetCallingAssembly();       // need to reset by every parsing
                _inlineParameterType = new Dictionary<string, Type>();   // need to reset by every parsing
                TokenStore declareTs;
                ParseInfo parseInfo = new ParseInfo();

                TokenStore ts = TokenStore.Parse(expression, out declareTs);
                if (ts != null && declareTs != null)
                {
                    //ParseDeclaration(declareTs);
                    parseInfo.localVariableStack.Push(new Dictionary<string, ParameterExpression>());
                    Assert(expression, ParseNextExpression(declareTs, null, parseInfo) == null, "Incorrect parameter declaration.");

                    var lv = parseInfo.localVariableStack.Pop();
                    foreach (string name in lv.Keys)
                    {
                        _inlineParameterType.Add(name, lv[name].Type);
                    }
                }

                return Expression.Lambda(ParseStatementBlock(ts, parseInfo, false), parseInfo.referredParameterList.ToArray());
            }
            catch (Exception ex)
            {
                throw new ExpressionParserException(expression, ex.Message, ex);
            }
        }

        public object Run(LambdaExpression lambda, params object[] inlineParamValues)
        {
            if (lambda == null) { throw new ArgumentNullException("lambda"); }

            Delegate f = lambda.Compile();
            if (inlineParamValues != null && inlineParamValues.Length > 0)
                return f.DynamicInvoke(inlineParamValues);

            List<object> parameterList = new List<object>();
            if (lambda.Parameters != null && lambda.Parameters.Count > 0)
            {
                foreach (ParameterExpression pe in lambda.Parameters)
                {
                    object value;
                    if (this._parameterValue.TryGetValue(pe.Name, out value))
                    {
                        parameterList.Add(value);
                    }
                    else if (GlobalParameterValue.TryGetValue(pe.Name, out value))
                    {
                        parameterList.Add(value);
                    }
                    else
                    {
                        throw new ExpressionParserException(string.Format("The value for {0} is undefined.", pe.Name));
                    }
                }
            }

            return f.DynamicInvoke(parameterList.ToArray());
        }

        private static void Assert(bool check, Token tok)
        {
            if (!check)
            {
                if (tok == null)
                    throw new ExpressionParserException("Unexpected end.");
                else
                    throw new ExpressionParserException(string.Format("Unexpected token({0}) at: {1}.", tok.Value, tok.StartPosition + 1));
            }
        }

        private static void Assert(bool check, string message)
        {
            if (!check)
                throw new ExpressionParserException(message);
        }

        private static void Assert(string expression, bool check, string message)
        {
            if (!check)
                throw new ExpressionParserException(expression, message);
        }

        private static void NormalizeMethodAndParamList(ref MethodInfo methodInfo, List<Expression> paramList)
        {
            if (methodInfo.IsGenericMethodDefinition)
            {
                // TODO - this currently support invoking generic method of type Object, need to
                // support specific param type
                methodInfo = methodInfo.MakeGenericMethod(methodInfo.GetGenericArguments().Select(t => typeof(object)).ToArray());
            }

            ParameterInfo[] pis = methodInfo.GetParameters();
            for (int i = 0; i < paramList.Count; i++)
            {
                Type paramType = pis[i].ParameterType;
                paramType = paramType.IsByRef ? paramType.GetElementType() : paramType;

                // check parameter implicit conversion
                if (paramList[i].Type != paramType &&
                    !paramList[i].Type.IsSubclassOrImplements(paramType))
                {
                    if (paramType.ContainsGenericParameters)
                    {
                        if (!paramType.IsGenericTypeDefinition)
                        {
                            paramType = paramType.GetGenericTypeDefinition();
                        }

                        // TODO - this currently support invoking generic method of type Object,
                        // need to support specific param type
                        paramType = paramType.MakeGenericType(paramType.GetGenericArguments().Select(t => typeof(object)).ToArray());
                    }

                    paramList[i] = Expression.Convert(paramList[i], paramType);
                }
            }
        }

        private static Expression ParseBreakStatement(TokenStore ts, ParseInfo parseInfo)
        {
            ts.Next();  // skip "break"
            JumpInfo jumpInfo = parseInfo.jumpInfoStack.Peek();
            if (jumpInfo.breakLabel == null)
            {
                jumpInfo.breakLabel = Expression.Label(Expression.Label());
            }

            Expression statement = Expression.Break(jumpInfo.breakLabel.Target);

            return statement;
        }

        private static Expression ParseContinueStatement(TokenStore ts, ParseInfo parseInfo)
        {
            ts.Next();  // skip "continue"
            JumpInfo jumpInfo = parseInfo.jumpInfoStack.Peek();
            if (jumpInfo.continueLabel == null)
            {
                jumpInfo.continueLabel = Expression.Label(Expression.Label());
            }

            Expression statement = Expression.Continue(jumpInfo.continueLabel.Target);

            return statement;
        }

        /// <summary>
        /// do statement while(test) : loop { statement; if(test) break; continueLabel } breakLabel
        /// </summary>
        /// <param name="ts"></param>
        /// <param name="parseInfo"></param>
        private Expression ParseDoWhileStatement(TokenStore ts, ParseInfo parseInfo)
        {
            ts.Next(); // skip "do"
            parseInfo.jumpInfoStack.Push(new JumpInfo());

            Expression statement = ParseNextStatement(ts, parseInfo);
            if (statement == null)
            {
                statement = Expression.Empty();
            }

            Assert(string.Equals(ts.CurrentValue, "while"), ts.Current);

            Token tok = ts.Next();
            Assert(tok != null && string.Equals(tok.Value, "("), tok); // skip "while"

            Expression test = ParseNextExpression(ts.NextUntilOperatorClose(), null, parseInfo);
            Assert(test != null, ts.Current);

            var jumpInfo = parseInfo.jumpInfoStack.Pop();
            if (jumpInfo.breakLabel == null)
            {
                jumpInfo.breakLabel = Expression.Label(Expression.Label());
            }

            statement = Expression.Loop(
                Expression.Block(
                    statement,
                    Expression.IfThenElse(test, Expression.Empty(), Expression.Break(jumpInfo.breakLabel.Target))),
                    jumpInfo.breakLabel.Target,
                    (jumpInfo.continueLabel == null) ? null : jumpInfo.continueLabel.Target);

            return statement;
        }

        /// <summary>
        /// foreach(exp1 var in exp2) statement : IEnumerator ie=exp2.GetEnumerator(); Loop {
        /// if(ie.MoveNext()) {var=ie.Current; statement;} continueLabel } breakLabel
        /// </summary>
        /// <param name="ts"></param>
        /// <param name="parseInfo"></param>
        private Expression ParseForeachStatement(TokenStore ts, ParseInfo parseInfo)
        {
            // begin stack
            var lv = new Dictionary<string, ParameterExpression>();
            parseInfo.localVariableStack.Push(lv);   // exp1 can contain local variable only for "for statement"
            parseInfo.jumpInfoStack.Push(new JumpInfo());

            Assert(ts.Next() != null && string.Equals(ts.CurrentValue, "("), ts.Current);  // skip "foreach" to "("
            TokenStore sub_ts = ts.NextUntilOperatorClose();

            // parse T
            Expression exp1 = ParseReferredParameterOrType(sub_ts, parseInfo, true, false, true);
            Assert(exp1 != null && exp1 is ConstantExpression && ((ConstantExpression)exp1).Value is Type, sub_ts.Current);
            Type t = (Type)((ConstantExpression)exp1).Value;

            // parse var
            Assert(sub_ts.Current != null && sub_ts.Current.TokenType == TokenType.IDENTIFIER, sub_ts.Current);    // to var
            ParameterExpression var = Expression.Variable(t, sub_ts.Current.Value);
            parseInfo.localVariableStack.Peek().Add(sub_ts.Current.Value, var);

            // parse resultExpr - IEnumerator
            Token tok = sub_ts.Next();
            Assert(tok != null && string.Equals(tok.Value, "in"), tok);
            sub_ts.Next();  // skip "in"

            Expression exp2 = ParseNextExpression(sub_ts, null, parseInfo);
            Assert(exp2 != null, (Token)null);

            // organize expressions
            ParameterExpression ie = Expression.Variable(typeof(IEnumerator));
            lv.Add("$$$$ie$$$$", ie);
            exp1 = Expression.Assign(ie, Expression.Call(exp2, "GetEnumerator", null, null));   // IEnumerator ie=exp2.GetEnumerator();
            Expression test = Expression.Call(ie, "MoveNext", null, null);     // ie.MoveNext()
            exp2 = Expression.Assign(var, Expression.Convert(Expression.Property(ie, "Current"), var.Type));  // var=ie.Current;

            // parse statement
            Expression statement = ParseNextStatement(ts, parseInfo);
            if (statement == null)
            {
                statement = Expression.Empty();
            }

            // end stack
            lv = parseInfo.localVariableStack.Pop();
            var jumpInfo = parseInfo.jumpInfoStack.Pop();
            if (jumpInfo.breakLabel == null)
            {
                jumpInfo.breakLabel = Expression.Label(Expression.Label());
            }

            // create foreach
            //exprList.Add(exp1);
            statement = Expression.Loop(Expression.IfThenElse(test, Expression.Block(exp2, statement), Expression.Break(jumpInfo.breakLabel.Target)), jumpInfo.breakLabel.Target, jumpInfo.continueLabel == null ? null : jumpInfo.continueLabel.Target);
            statement = Expression.Block(lv.Values, exp1, statement);

            return statement;
        }

        /// <summary>
        /// for(exp1; test; exp2) { statement; }
        /// 
        /// exp1; loop { if (test) statement; continuelabel; exp2; } breaklabel
        /// </summary>
        /// <param name="ts"></param>
        /// <param name="parseInfo"></param>
        private Expression ParseForStatement(TokenStore ts, ParseInfo parseInfo)
        {
            parseInfo.localVariableStack.Push(new Dictionary<string, ParameterExpression>());   // exp1 can contain local variable only for "for statement"
            parseInfo.jumpInfoStack.Push(new JumpInfo());

            Token tok = ts.Next();
            Assert(tok != null && string.Equals(tok.Value, "("), tok); // skip "for"

            TokenStore sub_ts = ts.NextUntilOperatorClose();
            Expression exp1 = ParseNextExpression(sub_ts, Operator.dicSystemOperator[";"], parseInfo);

            if (exp1 == null)
            {
                exp1 = Expression.Empty();
            }

            Assert(string.Equals(sub_ts.CurrentValue, ";"), sub_ts.Current);

            sub_ts.Next();  // skip ";"
            Expression test = ParseNextExpression(sub_ts, Operator.dicSystemOperator[";"], parseInfo);
            Assert(string.Equals(sub_ts.CurrentValue, ";"), sub_ts.Current);

            sub_ts.Next();  // skip ";"
            Expression exp2 = ParseNextExpression(sub_ts, null, parseInfo);

            Expression statement = ParseNextStatement(ts, parseInfo);
            if (statement == null)
            {
                statement = Expression.Empty();
            }

            var lv = parseInfo.localVariableStack.Pop();
            var jumpInfo = parseInfo.jumpInfoStack.Pop();

            if (jumpInfo.breakLabel == null)
            {
                jumpInfo.breakLabel = Expression.Label(Expression.Label());
            }

            //if (exp1 != null) exprList.Add(exp1);
            List<Expression> sub_exp_list = new List<Expression>();

            if (test != null) statement = Expression.IfThenElse(test, statement, Expression.Break(jumpInfo.breakLabel.Target));

            sub_exp_list.Add(statement);

            if (jumpInfo.continueLabel != null) sub_exp_list.Add(jumpInfo.continueLabel);
            if (exp2 != null) sub_exp_list.Add(exp2);

            statement = Expression.Loop(Expression.Block(sub_exp_list), jumpInfo.breakLabel.Target);
            statement = Expression.Block(lv.Values, exp1, statement);

            return statement;
        }

        /// <summary>
        /// if(test) statement else statement2
        /// </summary>
        /// <param name="ts"></param>
        /// <param name="parseInfo"></param>
        private Expression ParseIfStatement(TokenStore ts, ParseInfo parseInfo)
        {
            Token tok = ts.Next();
            Assert(tok != null && string.Equals(tok.Value, "("), tok);

            Expression test = ParseNextExpression(ts.NextUntilOperatorClose(), null, parseInfo);
            Assert(test != null && ts.Current != null, ts.Current);

            Expression statement = ParseNextStatement(ts, parseInfo);

            if (statement == null)
            {
                statement = Expression.Empty();
            }

            if (string.Equals(ts.CurrentValue, "else"))
            {
                ts.Next();  // skip "else"
                Expression statement2 = ParseNextStatement(ts, parseInfo);

                if (statement2 == null)
                {
                    statement2 = Expression.Empty();
                }

                statement = Expression.IfThenElse(test, statement, statement2);
            }
            else
            {
                statement = Expression.IfThen(test, statement);
            }

            return statement;
        }

        private Expression ParseMethodCallExpression(TokenStore ts, ParseInfo parseInfo, Expression resultExpr, Token tok)
        {
            List<Type> paramTypes = new List<Type>();
            List<Expression> paramList = ParseParameters(ts, parseInfo, paramTypes, false);

            if (resultExpr is ConstantExpression && ((ConstantExpression)resultExpr).Value is Type) // static call
            {
                Type type = (Type)(((ConstantExpression)resultExpr).Value);

                MethodInfo methodInfo = type.GetMethod(tok.Value, BindingFlags.Static | BindingFlags.Public, null, paramTypes.ToArray(), null);
                Assert(methodInfo != null, tok);

                NormalizeMethodAndParamList(ref methodInfo, paramList);

                resultExpr = Expression.Call(methodInfo, paramList.ToArray());
            }
            else // instance object method call
            {
                MethodInfo methodInfo = resultExpr.Type.GetMethod(tok.Value, BindingFlags.Instance | BindingFlags.Public, null, paramTypes.ToArray(), null);
                if (methodInfo == null)
                {
                    methodInfo = this.usingInstance.GetExtensionMethod(resultExpr.Type, tok.Value);
                    Assert(methodInfo != null, tok);

                    // Since it is an extension method, the first param has to be this
                    paramList.Insert(0, resultExpr);

                    NormalizeMethodAndParamList(ref methodInfo, paramList);

                    resultExpr = Expression.Call(methodInfo, paramList.ToArray());
                }
                else
                {
                    Assert(methodInfo != null, tok);

                    NormalizeMethodAndParamList(ref methodInfo, paramList);

                    resultExpr = Expression.Call(resultExpr, methodInfo, paramList.ToArray());
                }
            }

            return resultExpr;
        }

        private Expression ParseNextExpression(TokenStore ts, Operator leftOperator, ParseInfo parseInfo)
        {
            Expression resultExpr = null, exp2 = null, exp3 = null;
            MethodInfo methodInfo = null;
            Token tok;
            while ((tok = ts.Current) != null)
            {
                if (leftOperator != null && tok.Operator != null && tok.Operator.Precedence >= leftOperator.Precedence)
                {
                    return resultExpr;
                }

                // current operator has high Precedence than leftOp
                switch (tok.TokenType)
                {
                    case TokenType.INT: Assert(resultExpr == null, tok); resultExpr = Expression.Constant(int.Parse(tok.Value)); break;
                    case TokenType.UINT: Assert(resultExpr == null, tok); resultExpr = Expression.Constant(uint.Parse(tok.Value)); break;
                    case TokenType.LONG: Assert(resultExpr == null, tok); resultExpr = Expression.Constant(long.Parse(tok.Value)); break;
                    case TokenType.ULONG: Assert(resultExpr == null, tok); resultExpr = Expression.Constant(ulong.Parse(tok.Value)); break;
                    case TokenType.FLOAT: Assert(resultExpr == null, tok); resultExpr = Expression.Constant(float.Parse(tok.Value)); break;
                    case TokenType.DOUBLE: Assert(resultExpr == null, tok); resultExpr = Expression.Constant(double.Parse(tok.Value)); break;
                    case TokenType.DECIMAL: Assert(resultExpr == null, tok); resultExpr = Expression.Constant(decimal.Parse(tok.Value, NumberStyles.AllowExponent)); break;
                    case TokenType.BOOL: Assert(resultExpr == null, tok); resultExpr = Expression.Constant(bool.Parse(tok.Value)); break;
                    case TokenType.TEXT: Assert(resultExpr == null, tok); resultExpr = Expression.Constant(tok.Value); break;
                    case TokenType.CHAR: Assert(resultExpr == null, tok); resultExpr = Expression.Constant(string.IsNullOrEmpty(tok.Value) ? '\0' : tok.Value[0]); break;
                    case TokenType.IDENTIFIER:   // x, x[]

                        #region Parameter or Static Type reference

                        if (resultExpr != null && resultExpr is ConstantExpression && ((ConstantExpression)resultExpr).Value is Type) // static type
                        {
                            #region declare local variable: T var1=exp1, var2=exp2;

                            Type type = (Type)((ConstantExpression)resultExpr).Value;
                            Dictionary<string, ParameterExpression> localVaribles = parseInfo.localVariableStack.Peek();
                            List<Expression> exp_list = new List<Expression>();
                            while (ts.Current != null)
                            {
                                Assert(ts.Current.TokenType == TokenType.IDENTIFIER, ts.Current);
                                ParameterExpression var = Expression.Variable(type, ts.Current.Value);
                                localVaribles.Add(ts.Current.Value, var);
                                ts.Next();   // skip variable

                                if (string.Equals(ts.CurrentValue, "=")) // variable initializer
                                {
                                    ts.Next();  // skip "="
                                    if (var.Type.IsArray && string.Equals(ts.CurrentValue, "{")) // array initializer: T[] var = {1,2,3};
                                    {
                                        resultExpr = Expression.NewArrayInit(var.Type.GetElementType(), ParseParameters(ts, parseInfo, null, false));
                                    }
                                    else
                                    {
                                        resultExpr = ParseNextExpression(ts, Operator.dicSystemOperator[","], parseInfo);
                                    }

                                    if (resultExpr.Type != type)
                                    {
                                        resultExpr = Expression.Convert(resultExpr, type);
                                    }

                                    exp_list.Add(Expression.Assign(var, resultExpr));
                                }

                                if (ts.Current == null || string.Equals(ts.Current.Value, ";"))
                                {
                                    break;  // end of statement
                                }

                                if (string.Equals(ts.Current.Value, ","))
                                {
                                    ts.Next(); // skip "," to next variable
                                }
                            }

                            if (exp_list.Count == 0)
                            {
                                resultExpr = null;
                            }
                            else if (exp_list.Count == 1)
                            {
                                resultExpr = exp_list[0];
                            }
                            else
                            {
                                resultExpr = Expression.Block(exp_list);
                            }

                            #endregion declare local variable: T var1=exp1, var2=exp2;
                        }
                        else
                        {
                            #region Parameter or Static Type reference

                            Assert(resultExpr == null && (resultExpr = ParseReferredParameterOrType(ts, parseInfo, true, true, true)) != null, ts.Current);

                            #endregion Parameter or Static Type reference
                        }
                        break;

                        #endregion Parameter or Static Type reference

                    case TokenType.OPERATOR:

                        #region Operator

                        switch (tok.Operator.OperatorType)
                        {
                            case OperatorType.OPEN:

                                #region Open operators: (, [. "{" will be proccessed in ParseStatement()

                                switch (tok.Value)
                                {
                                    case "(":

                                        #region ParenthesisLeft: (nested expression)

                                        Assert(resultExpr == null, tok);
                                        resultExpr = ParseNextExpression(ts.NextUntilOperatorClose(), null, parseInfo);
                                        if (resultExpr is ConstantExpression && ((ConstantExpression)resultExpr).Value is Type) // (T)x
                                        {
                                            exp2 = ParseNextExpression(ts, Operator.dicSystemOperator["(T)"], parseInfo);
                                            if (exp2 != null)
                                                resultExpr = Expression.Convert(exp2, (Type)((ConstantExpression)resultExpr).Value);
                                        }
                                        break;

                                        #endregion ParenthesisLeft: (nested expression)

                                    case "[":

                                        #region BracketLeft: indexer[]

                                        Assert(resultExpr != null, tok);
                                        {
                                            List<Type> paramTypes = new List<Type>();
                                            List<Expression> paramList = ParseParameters(ts, parseInfo, paramTypes, false);

                                            //TokenStore subTs = ts.NextUntilOperatorClose();
                                            //List<Expression> paramList = new List<Expression>();
                                            //while ((exp2 = GetNextExpression(subTs, Operator.dicSystemOperator[","], exprParameterList)) != null)
                                            //{
                                            //    paramList.Add(exp2);
                                            //    subTs.Next();   // to ','
                                            //}
                                            ////List<TokenStore> ts_list = ts.NextUntilOperatorClose().Split(OPERATOR_NAME.Comma);
                                            ////List<Expression> paramList = new List<Expression>();
                                            ////foreach (TokenStore one_ts in ts_list)
                                            ////{
                                            ////    if(one_ts.tokenList.Count>0)
                                            ////        paramList.Add(GetNextExpression(one_ts, null, exprParameterList));
                                            ////}
                                            //List<Type> paramTypes = new List<Type>();
                                            //foreach (Expression e in paramList) paramTypes.Add(e.Type);
                                            if ((methodInfo = resultExpr.Type.GetMethod(tok.Operator.OverloadName, BindingFlags.Instance | BindingFlags.Public, null, paramTypes.ToArray(), null)) != null)
                                                resultExpr = Expression.Call(resultExpr, methodInfo, paramList.ToArray());
                                            else
                                                resultExpr = Expression.ArrayIndex(resultExpr, paramList.ToArray());
                                        }
                                        break;

                                        #endregion BracketLeft: indexer[]

                                    default:

                                        Assert(false, tok);
                                        break;
                                }
                                break;

                                #endregion Open operators: (, [. "{" will be proccessed in ParseStatement()

                            case OperatorType.PREFIX_UNARY:

                                #region prefix unary operators

                                try
                                {
                                    Assert(resultExpr == null && ts.Next() != null, tok);
                                    Assert((exp2 = ParseNextExpression(ts, tok.Operator, parseInfo)) != null, ts.Current);
                                    methodInfo = tok.Operator.OverloadName == null ? null : exp2.Type.GetMethod(tok.Operator.OverloadName, new Type[] { exp2.Type });
                                    resultExpr = ((d1m)tok.Operator.ExpressionCall)(exp2, methodInfo);
                                }
                                catch (Exception)
                                {
                                }
                                break;

                                #endregion prefix unary operators

                            case OperatorType.POST_UNARY:

                                #region POST_UNARY

                                try
                                {
                                    Assert(resultExpr != null, tok);
                                    methodInfo = tok.Operator.OverloadName == null ? null : resultExpr.Type.GetMethod(tok.Operator.OverloadName, new Type[] { resultExpr.Type });
                                    resultExpr = ((d1m)tok.Operator.ExpressionCall)(resultExpr, methodInfo);
                                }
                                catch (Exception)
                                {
                                }

                                break;

                                #endregion POST_UNARY

                            case OperatorType.BINARY:
                            case OperatorType.ASSIGN:

                                #region Binary or Assign Operators

                                try
                                {
                                    Assert(resultExpr != null && ts.Next() != null && (exp2 = ParseNextExpression(ts, tok.Operator, parseInfo)) != null, tok);
                                    methodInfo = null;
                                    Type type = null;
                                    if (string.Equals(tok.Value, "is"))
                                    {
                                        try
                                        {
                                            Assert(exp2 is ConstantExpression && ((ConstantExpression)exp2).Value is Type, tok);
                                            resultExpr = Expression.TypeIs(resultExpr, (Type)((ConstantExpression)exp2).Value);
                                        }
                                        catch (Exception)
                                        {
                                            resultExpr = Expression.Constant(false);
                                        }
                                        break;
                                    }
                                    else if (string.Equals(tok.Value, "as"))
                                    {
                                        Assert(exp2 is ConstantExpression && ((ConstantExpression)exp2).Value is Type, tok);
                                        resultExpr = Expression.TypeAs(resultExpr, (Type)((ConstantExpression)exp2).Value);
                                        break;
                                    }
                                    if (string.Equals(tok.Value, "+") && (resultExpr.Type == typeof(string) || exp2.Type == typeof(string)))
                                    {
                                        if (resultExpr.Type != typeof(string))
                                        {
                                            resultExpr = Expression.Convert(resultExpr, typeof(string));
                                        }

                                        if (exp2.Type != typeof(string))
                                        {
                                            exp2 = Expression.Convert(exp2, typeof(string));
                                        }

                                        methodInfo = typeof(string).GetMethod("Concat", new Type[] { typeof(string), typeof(string) });
                                    }
                                    else
                                    {
                                        if (tok.Operator.OverloadName != null)
                                        {
                                            methodInfo = resultExpr.Type.GetMethod(tok.Operator.OverloadName, new Type[] { resultExpr.Type, exp2.Type });
                                            if (methodInfo == null)
                                            {
                                                methodInfo = exp2.Type.GetMethod(tok.Operator.OverloadName, new Type[] { resultExpr.Type, exp2.Type });
                                            }
                                        }
                                        if (methodInfo == null)
                                        {
                                            // use default operator, need implicit type conversion
                                            if (tok.Operator.Required1stType != null)
                                            {
                                                resultExpr = Expression.Convert(resultExpr, tok.Operator.Required1stType);
                                            }

                                            if (tok.Operator.Required2ndType != null)
                                            {
                                                exp2 = Expression.Convert(exp2, tok.Operator.Required2ndType);
                                            }

                                            if (tok.Operator.requiredOperandType == RequiredOperandType.SAME
                                                && resultExpr.Type != exp2.Type
                                                && (type = QueryImplicitConversionType(resultExpr, exp2)) != null)
                                            {
                                                if (resultExpr.Type != type)
                                                {
                                                    resultExpr = Expression.Convert(resultExpr, type);
                                                }

                                                if (exp2.Type != type)
                                                {
                                                    exp2 = Expression.Convert(exp2, type);
                                                }
                                            }
                                        }
                                    }

                                    if (tok.Operator.ExpressionCall is d2)
                                    {
                                        resultExpr = ((d2)tok.Operator.ExpressionCall)(resultExpr, exp2);
                                    }
                                    else if (tok.Operator.ExpressionCall is d2m)
                                    {
                                        resultExpr = ((d2m)tok.Operator.ExpressionCall)(resultExpr, exp2, methodInfo);
                                    }
                                    else
                                    {
                                        resultExpr = ((d2bm)tok.Operator.ExpressionCall)(resultExpr, exp2, false, methodInfo);
                                    }
                                }
                                catch (Exception)
                                {
                                    Type type = tok.Operator.ReturnType;
                                    resultExpr = Expression.Constant(type.GetDefaultValue(), type);
                                }

                                break;

                                #endregion Binary or Assign Operators

                            case OperatorType.CONDITIONAL:

                                #region Conditional

                                try
                                {
                                    Assert(resultExpr != null && ts.Next() != null && (exp2 = ParseNextExpression(ts, tok.Operator, parseInfo)) != null, tok);
                                    Assert(string.Equals(ts.CurrentValue, ":") && ts.Next() != null, tok);
                                    Assert((exp3 = ParseNextExpression(ts, tok.Operator, parseInfo)) != null, tok);
                                    Type type;
                                    if (exp2.Type != exp3.Type && (type = QueryImplicitConversionType(exp2, exp3)) != null)
                                    {
                                        if (exp2.Type != type) exp2 = Expression.Convert(exp2, type);
                                        if (exp3.Type != type) exp3 = Expression.Convert(exp3, type);
                                    }
                                    resultExpr = Expression.Condition(resultExpr, exp2, exp3);
                                }
                                catch (Exception)
                                {
                                    resultExpr = Expression.Constant(false);
                                }

                                break;

                                #endregion Conditional

                            case OperatorType.PRIMARY:

                                #region Primary:

                                switch (tok.Value)
                                {
                                    case "new":

                                        #region New

                                        try
                                        {
                                            Assert(resultExpr == null && ts.Next() != null && ts.Current.TokenType == TokenType.IDENTIFIER, tok); // skip "new"
                                            Assert((resultExpr = ParseReferredParameterOrType(ts, parseInfo, true, false, false)) != null, tok);
                                            Type type = (Type)((ConstantExpression)resultExpr).Value;
                                            Assert(ts.Current != null && string.Equals(ts.Current.Value, "(") ||
                                                string.Equals(ts.Current.Value, "["), ts.Current);    // next is '(' or '['
                                            if (string.Equals(ts.Current.Value, "("))   // new object().
                                            {
                                                List<Type> param_types = new List<Type>();
                                                List<Expression> paramList = ParseParameters(ts, parseInfo, param_types, false);

                                                ConstructorInfo ci = type.GetConstructor(BindingFlags.Instance | BindingFlags.Public, null, param_types.ToArray(), null);
                                                Assert(ci != null, ts.Current);
                                                // check parameter implicit conversion
                                                ParameterInfo[] pis = ci.GetParameters();
                                                for (int idx = 0; idx < paramList.Count; idx++)
                                                {
                                                    if (paramList[idx].Type != pis[idx].ParameterType)
                                                        paramList[idx] = Expression.Convert(paramList[idx], pis[idx].ParameterType);
                                                }
                                                resultExpr = Expression.New(ci, paramList.ToArray());
                                            }
                                            else // new int[x,y] or new type[]{x,y}
                                            {
                                                List<Expression> param_list = ParseParameters(ts, parseInfo, null, false);  // [x,y]
                                                if (string.Equals(ts.CurrentValue, "{"))   // array initializer {1,2,3}
                                                    resultExpr = Expression.NewArrayInit(type, ParseParameters(ts, parseInfo, null, false));   // new array[] {x,y}
                                                else
                                                    resultExpr = Expression.NewArrayBounds(type, param_list); // new array[x,y]
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            resultExpr = Expression.Constant(null);
                                        }

                                        break;

                                        #endregion New

                                    case "typeof":

                                        #region typeof

                                        try
                                        {
                                            Assert(resultExpr == null && ts.Next() != null && string.Equals(ts.Current.Value, "("), tok); // must be "("
                                            Assert((resultExpr = ParseNextExpression(ts.NextUntilOperatorClose(), null, parseInfo)) != null, tok);
                                            Assert(resultExpr is ConstantExpression && ((ConstantExpression)resultExpr).Value is Type, "Invalid parameter for typeof(T)"); // typeof(T)
                                        }
                                        catch (Exception)
                                        {
                                            resultExpr = Expression.Constant(typeof(object));
                                        }
                                        break;

                                        #endregion typeof

                                    case "sizeof":

                                        #region sizeof

                                        try
                                        {
                                            Assert(resultExpr == null, tok);
                                            Assert(ts.Next() != null && string.Equals(ts.Current.Value, "("), tok); // must be "("
                                            Assert((resultExpr = ParseNextExpression(ts.NextUntilOperatorClose(), null, parseInfo)) != null, tok);
                                            Assert(resultExpr is ConstantExpression
                                                && ((ConstantExpression)resultExpr).Value is Type
                                                && ((Type)((ConstantExpression)resultExpr).Value).IsValueType, "Invalid parameter for sizeof(T)"); // sizeof(T)
                                            resultExpr = Expression.Constant(System.Runtime.InteropServices.Marshal.SizeOf((Type)((ConstantExpression)resultExpr).Value));
                                        }
                                        catch (Exception)
                                        {
                                            resultExpr = Expression.Constant(-1);
                                        }
                                        break;

                                        #endregion sizeof

                                    case ".":

                                        #region Property, Field or method call

                                        try
                                        {
                                            Assert(resultExpr != null && (tok = ts.Next()) != null && tok.TokenType == TokenType.IDENTIFIER, tok); // skip "." => tok=variable
                                            ts.Next();  // skip variable

                                            if (string.Equals(ts.CurrentValue, "("))  // method call
                                            {
                                                resultExpr = ParseMethodCallExpression(ts, parseInfo, resultExpr, tok);
                                            }
                                            else if (resultExpr is ConstantExpression && ((ConstantExpression)resultExpr).Value is Type)
                                            {
                                                resultExpr = ParseStaticPropertyExpression(ts, parseInfo, resultExpr, tok);
                                            }
                                            else
                                            {
                                                resultExpr = ParsePropertyExpression(ts, parseInfo, resultExpr, tok);
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            resultExpr = Expression.Constant(null);
                                        }

                                        break;

                                        #endregion Property, Field or method call

                                    default:
                                        // ,(not in method and indexer) and ;(need ASP.NET4.0)
                                        Assert(false, tok);
                                        break;
                                }
                                break;

                                #endregion Primary:
                        }
                        break;

                        #endregion Operator

                    default:
                        Assert(false, tok);
                        break;
                }
                if (ts.Current == tok) ts.Next();
            }
            return resultExpr;
        }

        private Expression ParseNextStatement(TokenStore ts, ParseInfo parseInfo)
        {
            Expression statement = null;
            Token tok = ts.Current;
            if (tok.TokenType == TokenType.IDENTIFIER)
            {
                switch (tok.Value)
                {
                    case "if":
                        statement = ParseIfStatement(ts, parseInfo);
                        break;

                    case "while":
                        statement = ParseWhileStatement(ts, parseInfo);
                        break;

                    case "do":
                        statement = ParseDoWhileStatement(ts, parseInfo);
                        break;

                    case "for":
                        statement = ParseForStatement(ts, parseInfo);
                        break;

                    case "foreach":
                        statement = ParseForeachStatement(ts, parseInfo);
                        break;

                    case "break":
                        statement = ParseBreakStatement(ts, parseInfo);
                        break;

                    case "continue":
                        statement = ParseContinueStatement(ts, parseInfo);
                        break;

                    case "switch":
                        statement = ParseSwitchStatement(ts, parseInfo);
                        break;

                    case "try":
                        statement = ParseTryCatchStatement(ts, parseInfo);
                        break;

                    case "throw":
                        statement = ParseThrowStatement(ts, parseInfo);
                        break;

                    case "return":
                        statement = ParseReturnStatement(ts, parseInfo);
                        break;
                }
            }

            if (statement == null)
            {
                if (string.Equals(tok.Value, "{"))
                    statement = ParseStatementBlock(ts.NextUntilOperatorClose(), parseInfo, false);
                else
                    statement = ParseNextExpression(ts, Operator.dicSystemOperator[";"], parseInfo);
            }

            if (string.Equals(ts.CurrentValue, ";")) ts.Next();  // skip ";"
            return statement;
        }

        private List<Expression> ParseParameters(TokenStore ts, ParseInfo parseInfo, List<Type> paramTypes, bool typeByValue)
        {
            TokenStore subTs = ts.NextUntilOperatorClose();
            List<Expression> param_list = new List<Expression>();
            while (subTs.Current != null)
            {
                bool isRef = false;
                if (string.Equals(subTs.CurrentValue, "ref") || string.Equals(subTs.CurrentValue, "out"))
                {
                    isRef = true;
                    subTs.Next();
                }
                Expression e = ParseNextExpression(subTs, Operator.dicSystemOperator[","], parseInfo);
                param_list.Add(e);
                if (paramTypes != null)
                {
                    if (isRef)
                    {
                        if (typeByValue)
                            paramTypes.Add(((Type)((ConstantExpression)e).Value).MakeByRefType());
                        else
                            paramTypes.Add(e.Type.MakeByRefType());
                    }
                    else
                    {
                        if (typeByValue)
                            paramTypes.Add((Type)((ConstantExpression)e).Value);
                        else
                            paramTypes.Add(e.Type);
                    }
                }
                Assert(subTs.Current == null || string.Equals(subTs.CurrentValue, ",") && subTs.Next() != null, subTs.Current);    // skip ','
            }
            //if (paramTypes != null)
            //{
            //    if(typeByValue)
            //        foreach (Expression e in paramList) paramTypes.Add((Type)((ConstantExpression)e).Value);
            //    else
            //        foreach (Expression e in paramList) paramTypes.Add(e.Type);
            //}
            return param_list;
        }

        private Expression ParsePropertyExpression(TokenStore ts, ParseInfo parseInfo, Expression resultExpr, Token tok)
        {
            PropertyInfo pi = null;
            FieldInfo fi = null;
            if ((pi = resultExpr.Type.GetProperty(tok.Value, BindingFlags.Instance | BindingFlags.Public)) != null)
                resultExpr = Expression.Property(resultExpr, pi);
            else if ((fi = resultExpr.Type.GetField(tok.Value, BindingFlags.Instance | BindingFlags.Public)) != null)
                resultExpr = Expression.Field(resultExpr, fi);
            else if (string.Equals(ts.CurrentValue, "<"))
            {
                // v1.1: Instance Generic method call
                List<Type> param_types = new List<Type>();
                List<Expression> paramList = ParseParameters(ts, parseInfo, param_types, true);

                MethodInfo methodInfo = resultExpr.Type.GetMethod(tok.Value, BindingFlags.Instance | BindingFlags.Public).MakeGenericMethod(param_types.ToArray());
                Assert(methodInfo != null, tok);
                //Assert((methodInfo = methodInfo.MakeGenericMethod(paramTypes.ToArray())) != null, tok);
                Assert(string.Equals(ts.CurrentValue, "("), ts.Current);    // to "("
                paramList = ParseParameters(ts, parseInfo, null, false);
                resultExpr = Expression.Call(resultExpr, methodInfo, paramList.ToArray());
            }
            else
                Assert(false, tok);

            return resultExpr;
        }

        private Expression ParseReferredParameterOrType(TokenStore ts, ParseInfo parseInfo, bool allowType, bool allowVariable, bool allowArrayType)
        {
            Type type = null;
            Expression exp = null;
            StringBuilder varFullName = new StringBuilder();
            Token tok = ts.Current;

            if (tok.Value == "null" && tok.TokenType == TokenType.IDENTIFIER)
            {
                return Expression.Constant(null);
            }

            while (exp == null && tok != null && tok.TokenType == TokenType.IDENTIFIER)
            {
                varFullName.Append(tok.Value);
                // try local variable
                if (allowVariable && (exp = parseInfo.GetReferredVariable(varFullName.ToString())) != null) { ts.Next(); break; }
                // try referred parameter
                else if (allowVariable && (type = this.QueryParameterType(varFullName.ToString())) != null)
                {
                    exp = parseInfo.GetReferredParameter(type, tok.Value);
                    ts.Next();
                    break;
                }
                // try static Generic Type
                else if (allowType && string.Equals(ts.PeekTokenValue(1), "<"))
                {
                    // v1.1: Generic Type reference
                    tok = ts.Next();    // to "<"
                    List<Type> paramTypes = new List<Type>();
                    List<Expression> paramList = ParseParameters(ts, parseInfo, paramTypes, true);
                    Assert((type = this.QueryStaticType(varFullName.Append("`").Append(paramList.Count).ToString()).MakeGenericType(paramTypes.ToArray())) != null, tok);
                    type = ParseStaticTypeExtension(ts, type, allowArrayType);
                    exp = Expression.Constant(type);   // Type: (T)x or T.xxx
                    break;
                }
                // try static Type
                else if (allowType && (type = this.QueryStaticType(varFullName.ToString())) != null)
                {
                    // Static Type reference
                    ts.Next();
                    type = ParseStaticTypeExtension(ts, type, allowArrayType);
                    exp = Expression.Constant(type);
                    break;
                }
                // neither parameter or static type
                if ((tok = ts.Next()) == null || tok.Value != ".") break;   // unexpected
                varFullName.Append(".");
                tok = ts.Next();    // skip "."
            }
            return exp;
        }

        /// <summary>
        /// </summary>
        /// <param name="ts"></param>
        /// <param name="parseInfo"></param>
        /// <returns></returns>
        private Expression ParseReturnStatement(TokenStore ts, ParseInfo parseInfo)
        {
            Expression statement = null;

            ts.Next();  // skip "return"
            Expression exp1 = ParseNextExpression(ts, Operator.dicSystemOperator[";"], parseInfo);
            if (exp1 == null)
            {
                if (parseInfo.returnLabel == null)
                {
                    parseInfo.returnLabel = Expression.Label(Expression.Label());
                }

                statement = Expression.Return(parseInfo.returnLabel.Target);
            }
            else
            {
                if (exp1.Type != typeof(object))
                {
                    exp1 = Expression.Convert(exp1, typeof(object));   // convert all return Value to object in case return different type
                }

                if (parseInfo.returnLabel == null)
                {
                    parseInfo.returnLabel = Expression.Label(Expression.Label(exp1.Type), Expression.Default(exp1.Type));
                }

                statement = Expression.Return(parseInfo.returnLabel.Target, exp1);
            }

            return statement;
        }

        private Expression ParseStatementBlock(TokenStore ts, ParseInfo parseInfo, bool isVoid)
        {
            List<Expression> exprList = new List<Expression>();
            parseInfo.localVariableStack.Push(new Dictionary<string, ParameterExpression>());
            //Token tok;
            //Expression test, exp1, exp2, statement, statement2;
            if (ts != null)
            {
                while (ts.Current != null)
                {
                    Expression statement = ParseNextStatement(ts, parseInfo);
                    if (statement != null)
                    {
                        exprList.Add(statement);
                    }
                }
            }

            if (isVoid && exprList.Count > 0 && exprList[exprList.Count - 1].Type != typeof(void))
            {
                exprList.Add(Expression.Empty());
            }

            var localVariable = parseInfo.localVariableStack.Pop();
            if (parseInfo.localVariableStack.Count == 0 && parseInfo.returnLabel != null) // this is the initial block, add return label if has been referred inside the block.
            {
                exprList.Add(parseInfo.returnLabel);
            }

            if (exprList.Count == 0)
            {
                return Expression.Empty();    //.Constant(null);
            }
            else if (exprList.Count == 1 && localVariable.Count == 0)
            {
                return exprList[0];
            }
            else
            {
                return Expression.Block(localVariable.Values, exprList.ToArray());
            }
        }

        private Expression ParseStaticPropertyExpression(TokenStore ts, ParseInfo parseInfo, Expression resultExpr, Token tok)
        {
            PropertyInfo pi = null;
            FieldInfo fi = null;
            Type type = (Type)(((ConstantExpression)resultExpr).Value);
            if ((pi = type.GetProperty(tok.Value, BindingFlags.Public | BindingFlags.Static)) != null)
                resultExpr = Expression.Property(null, pi);
            else if ((fi = type.GetField(tok.Value, BindingFlags.Public | BindingFlags.Static)) != null)
                resultExpr = Expression.Field(null, fi);
            else if ((pi = resultExpr.Type.GetProperty(tok.Value, BindingFlags.Instance | BindingFlags.Public)) != null)
                resultExpr = Expression.Property(resultExpr, pi);
            else if ((fi = resultExpr.Type.GetField(tok.Value, BindingFlags.Instance | BindingFlags.Public)) != null)
                resultExpr = Expression.Field(resultExpr, fi);
            else if (string.Equals(ts.CurrentValue, "<"))
            {
                // v1.1: Static Generic Method call
                List<Type> param_types = new List<Type>();
                List<Expression> paramList = ParseParameters(ts, parseInfo, param_types, true);

                MethodInfo methodInfo = methodInfo = type.GetMethod(tok.Value, BindingFlags.Static | BindingFlags.Public).MakeGenericMethod(param_types.ToArray());
                Assert(methodInfo != null, tok);

                //Assert((methodInfo = methodInfo.MakeGenericMethod(paramTypes.ToArray())) != null, tok);
                Assert(string.Equals(ts.CurrentValue, "("), ts.Current);    // to "("

                paramList = ParseParameters(ts, parseInfo, null, false);
                resultExpr = Expression.Call(methodInfo, paramList.ToArray());
            }
            else
                Assert(false, tok);

            return resultExpr;
        }

        private Type ParseStaticTypeExtension(TokenStore ts, Type type, bool allowArrayType)
        {
            // check nullable
            Type tempType;
            if (type.IsValueType &&
                string.Equals(ts.CurrentValue, "?") &&
                NullableTypes.TryGetValue(type, out tempType))
            {
                type = tempType;
                ts.Next();  // skip "?"
            }

            // check nested type
            while (string.Equals(ts.CurrentValue, ".") &&
                   ts.PeekToken(1) != null &&
                   ts.PeekToken(1).TokenType == TokenType.IDENTIFIER &&
                   (tempType = type.GetNestedType(ts.PeekToken(1).Value)) != null)
            {
                type = tempType;
                ts.Next();  // skip '.'
                ts.Next();  // skip nested type
            }

            // check array: type[,][], if after new operater, do not check array here
            if (allowArrayType)
            {
                while (string.Equals(ts.CurrentValue, "["))
                {
                    StringBuilder arr = new StringBuilder("[");
                    Token tok = ts.Next(); // skip "["
                    while ((tok = ts.Current) != null && !string.Equals(tok.Value, "]"))
                    {
                        arr.Append(tok.Value);
                    }

                    type = type.Assembly.GetType(type.FullName + arr.Append("]").ToString());

                    Assert(type != null, tok);
                    Assert(string.Equals(tok.Value, "]"), tok);
                    tok = ts.Next();  // skip "]"
                }
            }

            return type;
        }

        /// <summary>
        /// switch(test) { case test_values: case_statements; default:default_statements; }
        /// </summary>
        /// <param name="ts"></param>
        /// <param name="parseInfo"></param>
        private Expression ParseSwitchStatement(TokenStore ts, ParseInfo parseInfo)
        {
            // start stack
            parseInfo.localVariableStack.Push(new Dictionary<string, ParameterExpression>());
            parseInfo.jumpInfoStack.Push(new JumpInfo());

            // parse resultExpr
            Assert(ts.Next() != null && string.Equals(ts.CurrentValue, "("), ts.Current);  // skip "switch" to "("

            Expression test = ParseNextExpression(ts.NextUntilOperatorClose(), null, parseInfo);
            Assert(test != null, ts.Current);

            // parse case list
            Assert(string.Equals(ts.CurrentValue, "{"), ts.Current);
            TokenStore sub_ts = ts.NextUntilOperatorClose();
            List<SwitchCase> case_list = new List<SwitchCase>();
            List<Expression> test_values = new List<Expression>();
            List<Expression> case_statements = new List<Expression>();
            Expression default_statements = null;
            bool isDefault = false;
            Token tok;
            Expression statement;

            while ((tok = sub_ts.Current) != null)
            {
                if (string.Equals(tok.Value, "case"))
                {
                    Assert(!isDefault, tok);
                    if (test_values.Count > 0 && case_statements.Count > 0)
                    {
                        case_list.Add(Expression.SwitchCase(Expression.Block(case_statements), test_values));
                        case_statements.Clear();
                        test_values.Clear();
                    }

                    sub_ts.Next();  // skip "case"
                    test_values.Add(ParseNextExpression(sub_ts, Operator.dicSystemOperator[":"], parseInfo));
                    Assert(string.Equals(sub_ts.CurrentValue, ":") && sub_ts.Next() != null, sub_ts.Current);  // skip :
                }
                else if (string.Equals(tok.Value, "default"))
                {
                    isDefault = true;
                    if (test_values.Count > 0 && case_statements.Count > 0)
                    {
                        case_list.Add(Expression.SwitchCase(Expression.Block(case_statements), test_values));
                        case_statements.Clear();
                        test_values.Clear();
                    }
                    Assert(sub_ts.Next() != null && string.Equals(sub_ts.CurrentValue, ":") && sub_ts.Next() != null, sub_ts.Current);  // skip "default" and ":"
                }
                else
                {
                    statement = ParseNextStatement(sub_ts, parseInfo);
                    if (statement != null) case_statements.Add(statement);
                }
            }
            if (case_statements.Count > 0)
            {
                if (test_values.Count > 0)
                    case_list.Add(Expression.SwitchCase(Expression.Block(case_statements), test_values));
                if (isDefault)
                    default_statements = Expression.Block(case_statements);
            }
            // end stack
            var lv = parseInfo.localVariableStack.Pop();
            var jumpInfo = parseInfo.jumpInfoStack.Pop();
            if (jumpInfo.breakLabel == null)
            {
                jumpInfo.breakLabel = Expression.Label(Expression.Label());
            }

            // create switch
            statement = Expression.Switch(test, default_statements, case_list.ToArray());
            statement = Expression.Block(lv.Values, statement, jumpInfo.breakLabel);

            return statement;
        }

        /// <summary>
        /// </summary>
        /// <param name="ts"></param>
        /// <param name="parseInfo"></param>
        /// <returns></returns>
        private Expression ParseThrowStatement(TokenStore ts, ParseInfo parseInfo)
        {
            Expression statement, statement2;
            ts.Next();  // skip "throw"
            statement2 = ParseNextExpression(ts, Operator.dicSystemOperator[";"], parseInfo);
            if (statement2 == null)
                statement = Expression.Rethrow();
            else
                statement = Expression.Throw(statement2);

            return statement;
        }

        /// <summary>
        /// try { statement } catch(T var) { statement2 } finally { statement2 }
        /// </summary>
        /// <param name="ts"></param>
        /// <param name="parseInfo"></param>
        /// <param name="statement"></param>
        /// <param name="tok"></param>
        private Expression ParseTryCatchStatement(TokenStore ts, ParseInfo parseInfo)
        {
            Expression statement, statement2;

            // parse try block
            Token tok = ts.Next();
            Assert(tok != null && string.Equals(tok.Value, "{"), tok);  // skip "try" to "{"

            statement = ParseStatementBlock(ts.NextUntilOperatorClose(), parseInfo, true);
            if (statement == null)
            {
                statement = Expression.Empty();
            }

            // parse catch blocks
            List<CatchBlock> catch_list = new List<CatchBlock>();
            while (string.Equals(ts.CurrentValue, "catch"))
            {
                ts.Next();  // skip "catch"
                bool hasParameter = false;
                if (string.Equals(ts.CurrentValue, "("))
                {
                    hasParameter = true;
                    parseInfo.localVariableStack.Push(new Dictionary<string, ParameterExpression>());
                    Assert(ParseNextExpression(ts.NextUntilOperatorClose(), null, parseInfo) == null && parseInfo.localVariableStack.Peek().Count == 1, "Invalid syntax for catch."); // can only have one variable declare and no Value assignment
                }

                Assert(string.Equals(ts.CurrentValue, "{"), ts.Current);
                statement2 = ParseStatementBlock(ts.NextUntilOperatorClose(), parseInfo, true);
                if (statement2 == null)
                {
                    statement2 = Expression.Empty();
                }

                if (hasParameter)
                {
                    Dictionary<string, ParameterExpression> dic = parseInfo.localVariableStack.Pop();
                    foreach (string key in dic.Keys)    // there is only one key
                    {
                        catch_list.Add(Expression.Catch(dic[key], statement2));
                    }
                }
                else
                {
                    catch_list.Add(Expression.Catch(typeof(Exception), statement2));
                }
            }

            // parse finally block
            statement2 = null;
            if (string.Equals(ts.CurrentValue, "finally"))
            {
                tok = ts.Next();
                Assert(tok != null && string.Equals(tok.Value, "{"), tok);  // skip "finally"

                statement2 = ParseStatementBlock(ts.NextUntilOperatorClose(), parseInfo, false);
                if (statement2 == null)
                {
                    statement2 = Expression.Empty();
                }
            }

            statement = Expression.TryCatchFinally(statement, statement2, catch_list.ToArray());

            return statement;
        }

        /// <summary>
        /// while(test) statement : loop { if(test) statement; continueLabel; } breakLabel;
        /// </summary>
        /// <param name="ts"></param>
        /// <param name="parseInfo"></param>
        private Expression ParseWhileStatement(TokenStore ts, ParseInfo parseInfo)
        {
            parseInfo.jumpInfoStack.Push(new JumpInfo());

            Token tok = ts.Next();
            Assert(tok != null && string.Equals(tok.Value, "("), tok); // skip "while"

            Expression test = ParseNextExpression(ts.NextUntilOperatorClose(), null, parseInfo);
            Assert(test != null && ts.Current != null, ts.Current);

            Expression statement = ParseNextStatement(ts, parseInfo);

            if (statement == null)
            {
                statement = Expression.Empty();
            }

            JumpInfo jumpInfo = parseInfo.jumpInfoStack.Pop();

            if (jumpInfo.breakLabel == null)
            {
                jumpInfo.breakLabel = Expression.Label(Expression.Label());
            }

            statement = Expression.Loop(
                Expression.IfThenElse(test, statement, Expression.Break(jumpInfo.breakLabel.Target)),
                jumpInfo.breakLabel.Target,
                (jumpInfo.continueLabel == null) ? null : jumpInfo.continueLabel.Target);

            return statement;
        }

        private class Using
        {
            internal readonly Dictionary<string, Assembly> NamespaceList = new Dictionary<string, Assembly>();

            public Using()
            {
                Add("System");
            }

            public void Add(string namespaceName)
            {
                NamespaceList[namespaceName] = null;
            }

            public void Add(string namespaceName, string assemblyName)
            {
                var assemblies = AppDomain.CurrentDomain.GetAssemblies();
                foreach (Assembly assem in assemblies)
                {
                    string assemName = assem.FullName;
                    int indexOfComma = assemName.IndexOf(',');
                    if (indexOfComma > 0)
                    {
                        assemName = assemName.Substring(0, indexOfComma);
                    }

                    if (assem.FullName.Equals(assemblyName) ||
                        assemName.Equals(assemblyName, StringComparison.OrdinalIgnoreCase))
                    {
                        NamespaceList[namespaceName] = assem;
                        return;
                    }
                }

                throw new ExpressionParserException("Can't find assembly.");
            }

            public MethodInfo GetExtensionMethod(Type type, string methodName)
            {
                foreach (Assembly assembly in this.NamespaceList.Values.Where(a => a != null))
                {
                    foreach (MethodInfo mi in assembly.GetExtensionMethods(type))
                    {
                        if (string.Equals(mi.Name, methodName))
                        {
                            return mi;
                        }
                    }
                }

                return null;
            }
        }
    }
}