﻿// Copyright © Quick Bridge Funding 2014
// 
// Dev: Nghi Nguyen (nnguyen)
// Test: Nghi Nguyen (nnguyen)

using Newtonsoft.Json;
using System;
using System.Globalization;

namespace Sigma.LendFoundry.Rules.Core.Runtime
{
    public class DataReferenceVariable
    {
        private bool deserializedResult = false;

        private dynamic result;

        /// <summary>
        /// Errors that occurred during tring to get the data reference result.
        /// </summary>
        public Exception Error { get; set; }

        /// <summary> Gets whether the result is an array.
        public bool IsArray { get; set; }

        /// <summary>
        /// The data reference that this result is for.
        /// </summary>
        public string Name { get; set; }

        public dynamic Result
        {
            get
            {
                if (!deserializedResult)
                {
                    this.result = DeserializeResult();
                    this.deserializedResult = true;
                }

                return this.result;
            }
        }

        /// <summary>
        /// The serialized result as a string. For Object and IsArray results, the string will be
        /// JSON and is expected to be deserialized first, except for Binary (byte array).
        /// </summary>
        public string SerializedResult { get; set; }

        /// <summary>
        /// The type of the result of the data reference (or each element if the result is an array).
        /// </summary>
        public TypeCode Type { get; set; }

        private dynamic DeserializeResult()
        {
            if (this.Error != null)
            {
                throw new PhoenixRulesException(
                    string.Format(CultureInfo.CurrentCulture, "Data reference '{0}' has an error: {1}",
                        this.Name, this.Error.Message), this.Error);
            }

            if (string.IsNullOrEmpty(this.SerializedResult))
            {
                return null;
            }

            if (this.IsArray)
            {
                object[] array = JsonConvert.DeserializeObject<object[]>(this.SerializedResult);
                return Array.ConvertAll(array, e => GetResult(e));
            }
            else
            {
                return GetResult(this.SerializedResult);
            }
        }

        private dynamic GetResult(dynamic result)
        {
            switch (this.Type)
            {
                case TypeCode.Boolean:
                    return Convert.ToBoolean(result);

                case TypeCode.Byte:
                    return Convert.ToByte(result);

                case TypeCode.Char:
                    return Convert.ToChar(result);

                case TypeCode.DateTime:
                    string s = result.ToString();
                    s = s.Trim(' ', '"'); 
                    return Convert.ToDateTime(s);

                case TypeCode.Empty:
                case TypeCode.DBNull:
                    return null;

                case TypeCode.Decimal:
                    return Convert.ToDecimal(result);

                case TypeCode.Double:
                    return Convert.ToDouble(result);

                case TypeCode.Int16:
                    return Convert.ToInt16(result);

                case TypeCode.Int32:
                    return Convert.ToInt32(result);

                case TypeCode.Int64:
                    return Convert.ToInt64(result);

                case TypeCode.Object:
                    return result == null ? null : JsonConvert.DeserializeObject(result.ToString());

                case TypeCode.SByte:
                    return Convert.ToSByte(result);

                case TypeCode.Single:
                    return Convert.ToSingle(result);

                case TypeCode.String:
                    return result;

                case TypeCode.UInt16:
                    return Convert.ToUInt16(result);

                case TypeCode.UInt32:
                    return Convert.ToUInt32(result);

                case TypeCode.UInt64:
                    return Convert.ToUInt64(result);

                default:
                    throw new InvalidOperationException("Unhandled Data reference type: " + this.Type);
            }
        }
    }
}