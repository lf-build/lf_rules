﻿using System;
using System.Text;

namespace Sigma.LendFoundry.Rules.Core.Runtime
{
    public class ExpressionParserException : ApplicationException
    {
        private readonly string expression;

        public ExpressionParserException(string message)
            : base(message)
        {
        }

        public ExpressionParserException(string expression, string message)
            : base(message)
        {
            this.expression = expression;
        }

        public ExpressionParserException(string expression, string message, Exception ex)
            : base(message, ex)
        {
            this.expression = expression;
        }

        public override string Message
        {
            get
            {
                if (string.IsNullOrEmpty(this.expression))
                {
                    return base.Message;
                }

                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Expression: " + this.expression);
                sb.Append(base.Message);

                return sb.ToString();
            }
        }

        public override string ToString()
        {
            if (string.IsNullOrEmpty(this.expression))
            {
                return base.ToString();
            }

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Expression: " + this.expression);
            sb.Append(base.ToString());

            return sb.ToString();
        }
    }
}