﻿// Copyright © Quick Bridge Funding 2014
// 
// Dev: Nghi Nguyen (nnguyen)
// Test: Nghi Nguyen (nnguyen)

using System;
using System.Collections.Generic;
using System.Text;

namespace Sigma.LendFoundry.Rules.Core.Runtime
{
    internal class Token
    {
        internal Token(int startPos/*, int end_pos*/, TokenType tokType, Operator op, string value)
        {
            this.Value = value;
            this.StartPosition = startPos;
            //this.end_pos = end_pos;
            this.TokenType = tokType;
            this.Operator = op;
        }

        public Operator Operator { get; private set; }

        public int StartPosition { get; private set; }

        //internal int end_pos;
        public TokenType TokenType { get; private set; }

        public string Value { get; private set; }

        public override string ToString()
        {
            return string.Format("{0} ({1}, {2}, {3})", this.Value, this.TokenType, this.StartPosition, this.Operator);
        }
    }

    internal class TokenStore
    {
        private int curTokenIndex;
        private string originalString;
        private List<Token> tokenList;

        private TokenStore(string exp_string, List<Token> tokenList)
        {
            this.originalString = exp_string;
            this.tokenList = tokenList;
            curTokenIndex = 0;
        }

        //internal bool IsEnd() { return curTokenIndex >= tokenList.Count; }
        internal Token Current { get { return curTokenIndex >= tokenList.Count ? null : tokenList[curTokenIndex]; } }

        internal string CurrentValue { get { return curTokenIndex >= tokenList.Count ? null : tokenList[curTokenIndex].Value; } }

        internal static TokenStore Parse(string exprString, out TokenStore declareTs)
        {
            List<Token> list = new List<Token>();
            List<Token> declareList = null;
            int startPos = 0;
            TokenType tokenType = TokenType.NONE;
            bool isHex = false;         // 0xFFFF
            bool isExponent = false;    // 1e10, 1E-2
            bool isVerbatim = false;    // @"xxx"
            char startQuotation = '"';
            StringBuilder textValueBuilder = new StringBuilder();

            // blank : c <= ' '
            // text: '"' number : 0-9 decimal : . variable : A-Z,a-z,_
            // operator: else
            for (int idx = 0; idx < exprString.Length; idx++)
            {
                char c = exprString[idx];
                char c2 = (idx + 1) < exprString.Length ? exprString[idx + 1] : (char)0;
                switch (tokenType)
                {
                    case TokenType.NONE:

                        #region NONE

                        if (c <= ' ') continue; // skip space
                        startPos = idx;
                        if (c == '/' && c2 == '/') { idx++; tokenType = TokenType.COMMENT; }
                        else if (c == '/' && c2 == '*') { idx++; tokenType = TokenType.COMMENT_BLOCK; }
                        else if (c == '@' && c2 == '"')
                        {
                            idx++;
                            startQuotation = c2;
                            tokenType = TokenType.TEXT;
                            isVerbatim = true;
                        }
                        else if (c == '"')
                        {
                            startQuotation = c;
                            tokenType = TokenType.TEXT;
                        }
                        else if (c == '\'')
                        {
                            startQuotation = c;
                            tokenType = TokenType.CHAR;
                        }
                        else if (c == '0' && (c2 == 'x' || c2 == 'X')) { idx++; tokenType = TokenType.INT; isHex = true; }
                        else if (c >= '0' && c <= '9') tokenType = TokenType.INT;
                        else if (c == '.')
                        {
                            if (c2 >= '0' && c2 <= '9')
                                tokenType = TokenType.DOUBLE;  // no leading digit number
                            else
                                tokenType = TokenType.OPERATOR; // (x).y; x[i].y
                        }
                        else if (c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z' || c == '_') tokenType = TokenType.IDENTIFIER;
                        else tokenType = TokenType.OPERATOR;
                        break;

                        #endregion NONE

                    case TokenType.COMMENT:    // // xxxx \n
                        if (c == '\n') tokenType = TokenType.NONE;  // skip eveything until new line;
                        break;

                    case TokenType.COMMENT_BLOCK:  // /* xxxx */
                        if (c == '*' && c2 == '/') { idx++; tokenType = TokenType.NONE; }   // skip eveything until "*/";
                        break;

                    case TokenType.CHAR:
                    case TokenType.TEXT:   // "xxxx"

                        #region TEXT

                        if (!isVerbatim && c == '\\')  // escape for string
                        {
                            idx++;  // use c2
                            switch (c2)
                            {
                                case '0': textValueBuilder.Append('\0'); break;   // \0
                                case 'n': textValueBuilder.Append('\n'); break;   // new line
                                case 't': textValueBuilder.Append('\t'); break;   // tab
                                case 'r': textValueBuilder.Append('\r'); break;   // carriage return
                                case 'a': textValueBuilder.Append('\a'); break;   // alert
                                case 'b': textValueBuilder.Append('\b'); break;   // back space
                                case 'v': textValueBuilder.Append('\v'); break;   // vertical tab
                                case 'f': textValueBuilder.Append('\f'); break;   // Form Feed
                                case 'x': // hexadecimal \xF \xFF \xFFF \xFFFF
                                case 'u': // unicode \uFFFF
                                case 'U': // unicode \UFFFFFFFF
                                    {
                                        int additionalDigits = 0;
                                        while (additionalDigits < (c2 == 'U' ? 8 : 4)
                                            && (c = exprString.Length > idx + additionalDigits + 1 ? exprString[idx + additionalDigits + 1] : (char)0) != (char)0
                                            && (c >= '0' && c <= '9' || c >= 'A' && c <= 'F' || c >= 'a' && c <= 'f'))
                                            additionalDigits++;
                                        if (additionalDigits == 0 || c2 == 'u' && additionalDigits < 4 || c2 == 'U' && additionalDigits < 8)
                                            throw new ExpressionParserException(exprString, "Expecting hexadecimal for operator: '\\" + c2 + "'");
                                        else
                                        {
                                            textValueBuilder.Append((char)Convert.ToInt16("0x" + exprString.Substring(idx + 1, additionalDigits), 16));
                                            idx += additionalDigits;
                                        }
                                    }
                                    break;

                                default: textValueBuilder.Append(c2); break;   // '\\', '\"', '\''
                            }
                        }
                        else if (isVerbatim && c == startQuotation && c2 == startQuotation) // escape for verbatim string
                        {
                            textValueBuilder.Append(c);
                            idx++;
                        }
                        else if (c == startQuotation)  // end text
                        {
                            list.Add(new Token(startPos, c == '"' ? TokenType.TEXT : TokenType.CHAR, null, textValueBuilder.ToString()));
                            textValueBuilder = new StringBuilder();
                            tokenType = TokenType.NONE;
                            isVerbatim = false;
                        }
                        else
                            textValueBuilder.Append(c);
                        break;

                        #endregion TEXT

                    case TokenType.INT:

                        #region INT

                        if (c >= '0' && c <= '9') { }
                        else if (isHex && (c >= 'a' && c <= 'f' || c >= 'A' && c <= 'F')) { }
                        else if (c == '.' && !isHex) tokenType = TokenType.DOUBLE;
                        else if ((c == 'e' || c == 'E') && !isHex) { tokenType = TokenType.DOUBLE; isExponent = true; }
                        else
                        {
                            string val = isHex ? Convert.ToUInt64(exprString.Substring(startPos, idx - startPos), 16).ToString() : exprString.Substring(startPos, idx - startPos);
                            if (c <= ' ')  // space, \type, \n, ... control charaters
                            {
                                list.Add(new Token(startPos, tokenType, null, val));
                                tokenType = TokenType.NONE;
                            }
                            else if (c == '/' && c2 == '/')
                            {
                                list.Add(new Token(startPos, tokenType, null, val));
                                tokenType = TokenType.COMMENT;
                                idx++;
                            }
                            else if (c == '/' && c2 == '*')
                            {
                                list.Add(new Token(startPos, tokenType, null, val));
                                tokenType = TokenType.COMMENT_BLOCK;
                                idx++;
                            }
                            else if (c == 'U' || c == 'u')
                            {
                                if (c2 == 'L' || c2 == 'l')
                                {
                                    list.Add(new Token(startPos, TokenType.ULONG, null, val));
                                    tokenType = TokenType.NONE;
                                    idx++;
                                }
                                else
                                {
                                    list.Add(new Token(startPos, TokenType.UINT, null, val));
                                    tokenType = TokenType.NONE;
                                }
                            }
                            else if (c == 'L' || c == 'l')
                            {
                                if (c2 == 'U' || c2 == 'u')
                                {
                                    list.Add(new Token(startPos, TokenType.ULONG, null, val));
                                    tokenType = TokenType.NONE;
                                    idx++;
                                }
                                else
                                {
                                    list.Add(new Token(startPos, TokenType.LONG, null, val));
                                    tokenType = TokenType.NONE;
                                }
                            }
                            else if (!isHex && (c == 'F' || c == 'f'))
                            {
                                list.Add(new Token(startPos, TokenType.FLOAT, null, exprString.Substring(startPos, idx - startPos)));
                                tokenType = TokenType.NONE;
                            }
                            else if (!isHex && (c == 'D' || c == 'd'))
                            {
                                list.Add(new Token(startPos, TokenType.DOUBLE, null, exprString.Substring(startPos, idx - startPos)));
                                tokenType = TokenType.NONE;
                            }
                            else if (!isHex && (c == 'M' || c == 'm'))
                            {
                                list.Add(new Token(startPos, TokenType.DECIMAL, null, exprString.Substring(startPos, idx - startPos)));
                                tokenType = TokenType.NONE;
                            }
                            else if (c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z' || c == '_')
                            {
                                list.Add(new Token(startPos, tokenType, null, val));
                                startPos = idx;
                                tokenType = TokenType.IDENTIFIER; // report error later
                            }
                            else
                            {
                                // operator, add previous token
                                list.Add(new Token(startPos, tokenType, null, val));
                                startPos = idx;
                                tokenType = TokenType.OPERATOR;
                            }
                            isHex = false;
                        }
                        break;

                        #endregion INT

                    case TokenType.DOUBLE:

                        #region DOUBLE

                        if (c >= '0' && c <= '9') { }
                        else if ((c == '-' || c == '+') && isExponent) { }  // 1e-2. can only have one sign
                        else
                        {
                            if (c <= ' ')  // space, \type, \n, ... control charaters
                            {
                                list.Add(new Token(startPos, tokenType, null, exprString.Substring(startPos, idx - startPos)));
                                tokenType = TokenType.NONE;
                            }
                            else if (c == '/' && c2 == '/')
                            {
                                list.Add(new Token(startPos, tokenType, null, exprString.Substring(startPos, idx - startPos)));
                                tokenType = TokenType.COMMENT;
                                idx++;
                            }
                            else if (c == '/' && c2 == '*')
                            {
                                list.Add(new Token(startPos, tokenType, null, exprString.Substring(startPos, idx - startPos)));
                                tokenType = TokenType.COMMENT_BLOCK;
                                idx++;
                            }
                            else if (c == 'F' || c == 'f')
                            {
                                list.Add(new Token(startPos, TokenType.FLOAT, null, exprString.Substring(startPos, idx - startPos)));
                                tokenType = TokenType.NONE;
                            }
                            else if (c == 'D' || c == 'd')
                            {
                                list.Add(new Token(startPos, TokenType.DOUBLE, null, exprString.Substring(startPos, idx - startPos)));
                                tokenType = TokenType.NONE;
                            }
                            else if (c == 'M' || c == 'm')
                            {
                                list.Add(new Token(startPos, TokenType.DECIMAL, null, exprString.Substring(startPos, idx - startPos)));
                                tokenType = TokenType.NONE;
                            }
                            else if (c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z' || c == '_')
                            {
                                list.Add(new Token(startPos, tokenType, null, exprString.Substring(startPos, idx - startPos)));
                                startPos = idx;
                                tokenType = TokenType.IDENTIFIER; // report error later
                            }
                            else
                            {
                                // token change, save previous token
                                list.Add(new Token(startPos, tokenType, null, exprString.Substring(startPos, idx - startPos)));
                                startPos = idx;
                                tokenType = TokenType.OPERATOR;
                            }
                        }
                        isExponent = false; // can only have one sign right after 'e'
                        break;

                        #endregion DOUBLE

                    case TokenType.IDENTIFIER:

                        #region VARIABLE

                        // VARIABLE(x or ns.x) can be a token of
                        // - boolean: true / false => BOOL
                        // - null: null => TEXT
                        // - is => operator
                        // - new => operator
                        // - property: .x => VARIABLE. continue to see operator '[' and '(' for x[]
                        //   and x()
                        // - static type: ns.x => TYPE. continue to see operator '[' and '(' for x[]
                        //   and x()
                        // - parameter: x => VARIABLE
                        // - un-typed parameter: error => VARIABLE, will report error later
                        if (c >= '0' && c <= '9' || c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z' || c == '_') { }
                        else
                        {
                            string variable_name = exprString.Substring(startPos, idx - startPos);
                            if (string.Equals(variable_name, "true") || string.Equals(variable_name, "false")) // boolean
                                list.Add(new Token(startPos, TokenType.BOOL, null, variable_name));
                            else if (string.Equals(variable_name, "null"))  // null
                                list.Add(new Token(startPos, TokenType.TEXT, null, null));
                            else if (string.Equals(variable_name, "is") // is
                                || string.Equals(variable_name, "as")    // as
                                || string.Equals(variable_name, "new") // new
                                || string.Equals(variable_name, "typeof") // typeof
                                || string.Equals(variable_name, "sizeof") // sizeof
                                )
                                ParseOperatorTokens(startPos, variable_name, ref list, ref declareList);
                            else // un-typed parameter, will return error later
                                list.Add(new Token(startPos, tokenType, null, variable_name));

                            // set next token type
                            if (c <= ' ') tokenType = TokenType.NONE;
                            else if (c == '/' && c2 == '/')
                            {
                                tokenType = TokenType.COMMENT;
                                idx++;
                            }
                            else if (c == '/' && c2 == '*')
                            {
                                tokenType = TokenType.COMMENT_BLOCK;
                                idx++;
                            }
                            else
                                tokenType = TokenType.OPERATOR;
                            startPos = idx;
                        }
                        break;

                        #endregion VARIABLE

                    case TokenType.OPERATOR:

                        #region OPERATOR

                        if (c <= ' ')  // space, \type, \n, ... control charaters
                        {
                            ParseOperatorTokens(startPos, exprString.Substring(startPos, idx - startPos), ref list, ref declareList);
                            tokenType = TokenType.NONE;
                        }
                        else if (c == '/' && c2 == '/')
                        {
                            ParseOperatorTokens(startPos, exprString.Substring(startPos, idx - startPos), ref list, ref declareList);
                            tokenType = TokenType.COMMENT;
                            idx++;
                        }
                        else if (c == '/' && c2 == '*')
                        {
                            ParseOperatorTokens(startPos, exprString.Substring(startPos, idx - startPos), ref list, ref declareList);
                            tokenType = TokenType.COMMENT_BLOCK;
                            idx++;
                        }
                        else if (c == '0' && (c2 == 'x' || c2 == 'X'))
                        {
                            ParseOperatorTokens(startPos, exprString.Substring(startPos, idx - startPos), ref list, ref declareList);
                            startPos = idx;
                            isHex = true;
                            tokenType = TokenType.INT;
                            idx++;
                        }
                        else if (c >= '0' && c <= '9')
                        {
                            ParseOperatorTokens(startPos, exprString.Substring(startPos, idx - startPos), ref list, ref declareList);
                            startPos = idx;
                            tokenType = TokenType.INT;
                        }
                        else if (c == '@' && c2 == '"')
                        {
                            ParseOperatorTokens(startPos, exprString.Substring(startPos, idx - startPos), ref list, ref declareList);
                            idx++;
                            startPos = idx;    // start from " instead of @
                            tokenType = TokenType.TEXT;
                            startQuotation = c2;
                            isVerbatim = true;
                        }
                        else if (c == '"' || c == '\'')
                        {
                            ParseOperatorTokens(startPos, exprString.Substring(startPos, idx - startPos), ref list, ref declareList);
                            startPos = idx;
                            tokenType = c2 == '"' ? TokenType.TEXT : TokenType.CHAR;
                            startQuotation = c;
                        }
                        else if (c == '.' && c2 >= '0' && c2 <= '9')  // no leading digit decimal, like .999
                        {
                            ParseOperatorTokens(startPos, exprString.Substring(startPos, idx - startPos), ref list, ref declareList);
                            startPos = idx;
                            OperatorType previous_token_op_type = (list.Count > 0 && list[list.Count - 1].Operator != null) ? list[list.Count - 1].Operator.OperatorType : OperatorType.UNKNOWN;
                            if (previous_token_op_type == OperatorType.CLOSE)
                                tokenType = TokenType.OPERATOR; // (x).y; x[i].y
                            else
                                tokenType = TokenType.DOUBLE;  // no leading digit number
                        }
                        else if (c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z' || c == '_')
                        {
                            ParseOperatorTokens(startPos, exprString.Substring(startPos, idx - startPos), ref list, ref declareList);
                            startPos = idx;
                            tokenType = TokenType.IDENTIFIER;
                        }
                        else { } // continue operator
                        break;

                        #endregion OPERATOR
                }
            }
            if (tokenType == TokenType.COMMENT_BLOCK || tokenType == TokenType.TEXT)
                throw new ExpressionParserException(exprString, "Unexpected end of the expression.");
            else if (tokenType != TokenType.NONE && tokenType != TokenType.COMMENT)
            {
                if (tokenType == TokenType.OPERATOR)
                    ParseOperatorTokens(startPos, exprString.Substring(startPos, exprString.Length - startPos), ref list, ref declareList);
                else
                    list.Add(new Token(startPos, tokenType, null, exprString.Substring(startPos, exprString.Length - startPos)));
            }
            declareTs = declareList != null && declareList.Count > 0 ? new TokenStore(exprString, declareList) : null;
            return new TokenStore(exprString, list);
        }

        // for operator
        internal static void ParseOperatorTokens(int startPos, string operatorString, ref List<Token> list, ref List<Token> declareList)
        {
            int opStart = 0;
            int opLen = operatorString.Length;
            // need check Prefix Unary operator if there is no operand before or there is non-CLOSE
            // operator before
            Token lastToken = list.Count > 0 ? list[list.Count - 1] : null;
            bool checkPreUnary = lastToken == null || lastToken.Operator != null && lastToken.Operator.OperatorType != OperatorType.CLOSE;

            while (opLen > 0)
            {
                string opString = operatorString.Substring(opStart, opLen);
                Operator exprOperator = null;

                if (checkPreUnary && Operator.dicSystemOperator.ContainsKey(opString + "U"))
                    exprOperator = Operator.dicSystemOperator[opString + "U"];
                if (exprOperator == null && Operator.dicSystemOperator.ContainsKey(opString))
                    exprOperator = Operator.dicSystemOperator[opString];
                if (exprOperator != null)
                {
                    //if (opStr, "(" && lastToken != null && lastToken.tokenType == TokenType.VARIABLE) lastToken.tokenType = TokenType.FUNC;
                    Token tok = new Token(startPos + opStart, TokenType.OPERATOR, exprOperator, opString);
                    if (string.Equals(tok.Value, "=>") && declareList == null)
                    {
                        declareList = list;
                        list = new List<Token>();
                        lastToken = null;
                    }
                    else
                    {
                        list.Add(tok);
                        lastToken = tok;
                    }
                    opStart += opLen;
                    opLen = operatorString.Length - opStart;
                    checkPreUnary = exprOperator.OperatorType != OperatorType.CLOSE;
                }
                else
                {
                    opLen--;
                }
            }
            if (opLen == 0 && opStart < operatorString.Length)
            {
                // unknown operator
                string opStr = operatorString.Substring(opStart);
                list.Add(new Token(startPos + opStart, TokenType.OPERATOR, Operator.Unknown, opStr));
            }
        }

        //internal Token Previous { get { return curTokenIndex > 0 && tokenList.Count > 0 ? tokenList[curTokenIndex - 1] : null; } }
        internal Token Next()
        {
            if (curTokenIndex < tokenList.Count)
            {
                curTokenIndex++;
            }

            return curTokenIndex < tokenList.Count ? tokenList[curTokenIndex] : null;
        }

        internal TokenStore NextUntilOperatorClose()
        {
            Token startToken = this.Current;
            int level = 1;
            List<Token> list = new List<Token>();
            for (this.curTokenIndex++; this.curTokenIndex < this.tokenList.Count; this.curTokenIndex++)
            {
                Token curToken = this.tokenList[this.curTokenIndex];
                if (curToken.Operator != null)
                {
                    if (startToken.Value == curToken.Value) level++;
                    else if (string.Equals(startToken.Value, "(") && string.Equals(curToken.Value, ")") // ( )
                            || string.Equals(startToken.Value, "[") && string.Equals(curToken.Value, "]")       // [ ]
                            || string.Equals(startToken.Value, "{") && string.Equals(curToken.Value, "}")         // { }
                            || string.Equals(startToken.Value, "<") && string.Equals(curToken.Value, ">"))          // < >
                        level--;
                    else if (
                        string.Equals(startToken.Value, "<") &&
                        string.Equals(curToken.Value, ">>") &&
                        level >= 2)    // v<xxx<xx>>
                    {
                        level -= 2;
                        list.Add(new Token(curToken.StartPosition, TokenType.OPERATOR, Operator.dicSystemOperator[">"], ">"));
                        curToken = new Token(curToken.StartPosition + 1, TokenType.OPERATOR, Operator.dicSystemOperator[">"], ">");
                    }
                    if (level == 0) break;
                }

                list.Add(curToken);
            }

            if (level != 0)
            {
                throw new ExpressionParserException("Expecting closing operator for operator: '" + startToken.Value + "'");
            }

            this.curTokenIndex++;   // move to next token after close
            return new TokenStore(this.originalString, list);
        }

        internal Token PeekToken(int offsetIndex)
        {
            return (curTokenIndex + offsetIndex >= 0 && curTokenIndex + offsetIndex < tokenList.Count)
                ? tokenList[curTokenIndex + offsetIndex]
                : null;
        }

        internal string PeekTokenValue(int offsetIdx)
        {
            Token tok = this.PeekToken(offsetIdx);
            return tok == null ? null : tok.Value;
        }
    }
}