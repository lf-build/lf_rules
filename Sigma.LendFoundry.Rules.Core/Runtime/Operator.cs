﻿// Copyright © Quick Bridge Funding 2014
// 
// Dev: Nghi Nguyen (nnguyen)
// Test: Nghi Nguyen (nnguyen)

using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Sigma.LendFoundry.Rules.Core.Runtime
{
    internal class Operator
    {
        public static readonly Operator Unknown = new Operator(OperatorType.UNKNOWN, typeof(object), 0);

        internal static Dictionary<string, Operator> dicSystemOperator = CreateSystemOperatorsMap();

        // http: //msdn.microsoft.com/en-us/library/2sk3x8a7.aspx
        private Operator(OperatorType opType, Type returnType, int precedence)
            : this(opType, returnType, precedence, null, null, RequiredOperandType.NONE, null, null)
        {
        }

        private Operator(OperatorType opType, Type returnType, int precedence, Delegate exp_call, string overload_name)
            : this(opType, returnType, precedence, null, null, RequiredOperandType.NONE, exp_call, overload_name)
        {
        }

        private Operator(OperatorType opType, Type returnType, int precedence, Type required1stType, Type required2ndType, RequiredOperandType requiredOperandType, Delegate exp_call, string overloadName)
        {
            //this.op_name = op_name;
            this.OperatorType = opType;
            this.Precedence = precedence;
            this.Required1stType = required1stType;
            this.Required2ndType = required2ndType;
            this.requiredOperandType = requiredOperandType;
            this.ExpressionCall = exp_call;
            this.OverloadName = overloadName;
            this.ReturnType = returnType;
        }

        public Delegate ExpressionCall { get; private set; }

        //public OPERATOR_NAME op_name;
        public OperatorType OperatorType { get; private set; }

        public string OverloadName { get; private set; }

        public int Precedence { get; private set; }

        public Type Required1stType { get; private set; }

        public Type Required2ndType { get; private set; }

        public RequiredOperandType requiredOperandType { get; private set; }

        public Type ReturnType { get; private set; }

        internal static Dictionary<string, Operator> CreateSystemOperatorsMap()
        {
            Dictionary<string, Operator> dic = new Dictionary<string, Operator>();
            // special
            dic.Add(";", new Operator(OperatorType.PRIMARY, null, 999)); // Semicolon,   // end of statement; lowest priority
            dic.Add(",", new Operator(OperatorType.PRIMARY, null, 990));  // "op_Comma")); // end of parameter. ex. x,y
            dic.Add("[", new Operator(OperatorType.OPEN, null, 0, null, "get_Item")); // BracketLeft,     //a[x]	0.ArrayAccess; ArrayIndex; ArrayLength
            dic.Add("]", new Operator(OperatorType.CLOSE, null, 0)); // BracketRight,     //a[x]	0.ArrayAccess; ArrayIndex; ArrayLength
            dic.Add("(", new Operator(OperatorType.OPEN, null, 0)); // ParenthesisLeft,    //f(x) 0.Call; (T)x	10.??; () 200.??
            dic.Add(")", new Operator(OperatorType.CLOSE, null, 0)); // ParenthesisRight,    //f(x) 0.Call; (T)x	10.??; () 200.??
            dic.Add("{", new Operator(OperatorType.OPEN, null, 0)); // BlockLeft,      // { block; }
            dic.Add("}", new Operator(OperatorType.CLOSE, null, 0)); // BlockRight,      // { block; }
            dic.Add(".", new Operator(OperatorType.PRIMARY, null, 0)); // Dot,                           //x.y	0.FieldOrProperty; x.y(); x.y[]
            dic.Add("new", new Operator(OperatorType.PRIMARY, typeof(object), 0));    // new
            dic.Add("typeof", new Operator(OperatorType.PRIMARY, typeof(Type), 0));    // TypeOf
            //dic.Add("as", new Operator(OperatorType.PRIMARY, 0)); // var as T : Expression.TypeAs(resultExpr, Type)
            dic.Add("sizeof", new Operator(OperatorType.PRIMARY, typeof(int), 1));   // SizeOf, it should be PREFIX_UNARY

            dic.Add("=>", new Operator(OperatorType.PRIMARY, typeof(object), 0));  // LINQ goes to
            //[Unary] : U - unary operator, xU-prefix operator, only available when there is no previous operand;
            dic.Add("(T)", new Operator(OperatorType.PREFIX_UNARY, typeof(object), 1));   // Cast
            dic.Add("+U", new Operator(OperatorType.PREFIX_UNARY, typeof(object), 10, (d1m)Expression.UnaryPlus, "op_UnaryPlus")); // UnaryPlus,                     //+x		10.UnaryPlus			// Unary
            dic.Add("-U", new Operator(OperatorType.PREFIX_UNARY, typeof(object), 10, (d1m)Expression.Negate, "op_UnaryNegation")); // UnaryMinus,                    //-x		10.Negate				// Unary
            dic.Add("!U", new Operator(OperatorType.PREFIX_UNARY, typeof(bool), 10, (d1m)Expression.Not, null)); // UnaryLogicalNot,               //!		10.Not					// Unary
            dic.Add("~U", new Operator(OperatorType.PREFIX_UNARY, typeof(bool), 10, (d1m)Expression.Not, "op_OnesComplement")); // UnaryBitNot,                   //~		10.Not					// Unary
            dic.Add("--", new Operator(OperatorType.POST_UNARY, typeof(int), 0, (d1m)Expression.PostDecrementAssign, "op_Decrement")); // PostDecrementAssign,    ////x--		0.PostDecrementAssign	// Unary
            dic.Add("++", new Operator(OperatorType.POST_UNARY, typeof(int), 0, (d1m)Expression.PostIncrementAssign, "op_Increment")); // PostIncrementAssign,    ////x++		0.PostIncrementAssign	// Unary
            dic.Add("--U", new Operator(OperatorType.PREFIX_UNARY, typeof(int), 10, (d1m)Expression.PreDecrementAssign, null)); // PreDecrementAssign,     ////--x		10.PreDecrementAssign   // Unary
            dic.Add("++U", new Operator(OperatorType.PREFIX_UNARY, typeof(int), 10, (d1m)Expression.PreIncrementAssign, null)); // PreIncrementAssign,     ////++x		10.PreIncrementAssign   // Unary
            //[Binary]
            dic.Add("**", new Operator(OperatorType.BINARY, typeof(double), 15, typeof(double), typeof(double), RequiredOperandType.NONE, (d2)Expression.Power, null)); // Power,     //x**y	15.Power
            dic.Add("*", new Operator(OperatorType.BINARY, typeof(double), 20, null, null, RequiredOperandType.SAME, (d2m)Expression.Multiply, "op_Multiply")); // Multiply,     //x*y		20.Multiply
            dic.Add("/", new Operator(OperatorType.BINARY, typeof(double), 20, null, null, RequiredOperandType.SAME, (d2m)Expression.Divide, "op_Division")); // Divide,    //x/y		20.Divide
            dic.Add("%", new Operator(OperatorType.BINARY, typeof(double), 20, null, null, RequiredOperandType.SAME, (d2m)Expression.Modulo, "op_Modulus")); // Modulo,      //x%y		20.Modulo(Reminder)
            dic.Add("+", new Operator(OperatorType.BINARY, typeof(double), 30, null, null, RequiredOperandType.SAME, (d2m)Expression.Add, "op_Addition")); // Add,   //x+y		30.Add
            dic.Add("-", new Operator(OperatorType.BINARY, typeof(double), 30, null, null, RequiredOperandType.SAME, (d2m)Expression.Subtract, "op_Subtraction")); // Subtract,  //x-y		30.Subtract
            dic.Add("<<", new Operator(OperatorType.BINARY, typeof(double), 40, null, typeof(int), RequiredOperandType.NONE, (d2m)Expression.LeftShift, "op_LeftShift")); // LeftShift,   //x<<y	40.LeftShift
            dic.Add(">>", new Operator(OperatorType.BINARY, typeof(double), 40, null, typeof(int), RequiredOperandType.NONE, (d2m)Expression.RightShift, "op_RightShift")); // RightShift,    //x>>y	40.RightShift
            dic.Add(">", new Operator(OperatorType.BINARY, typeof(bool), 50, null, null, RequiredOperandType.SAME, (d2bm)Expression.GreaterThan, "op_GreaterThan")); // GreaterThan,   //x>y		50.GreaterThan
            dic.Add(">=", new Operator(OperatorType.BINARY, typeof(bool), 50, null, null, RequiredOperandType.SAME, (d2bm)Expression.GreaterThanOrEqual, "op_GreaterThanOrEqual")); // GreaterThanOrEqual,    //x>=y	50.GreaterThanOrEqual
            dic.Add("<", new Operator(OperatorType.BINARY, typeof(bool), 50, null, null, RequiredOperandType.SAME, (d2bm)Expression.LessThan, "op_LessThan")); // LessThan,  //x<y		50.LessThan
            dic.Add("<=", new Operator(OperatorType.BINARY, typeof(bool), 50, null, null, RequiredOperandType.SAME, (d2bm)Expression.LessThanOrEqual, "op_LessThanOrEqual")); // LessThanOrEqual,   //x<=y	50.LessThanOrEqual
            dic.Add("is", new Operator(OperatorType.BINARY, typeof(bool), 50)); // is,   //x is T	50.TypeIs
            dic.Add("as", new Operator(OperatorType.BINARY, typeof(object), 50)); // as,   //x as T	50.TypeAs
            dic.Add("==", new Operator(OperatorType.BINARY, typeof(bool), 60, null, null, RequiredOperandType.SAME, (d2bm)Expression.Equal, "op_Equality")); // Equal, //x==y	30.Equal
            dic.Add("!=", new Operator(OperatorType.BINARY, typeof(bool), 60, null, null, RequiredOperandType.SAME, (d2bm)Expression.NotEqual, "op_Inequality")); // NotEqual,  //x!=y	30.NotEqual
            dic.Add("&", new Operator(OperatorType.BINARY, typeof(object), 70, null, null, RequiredOperandType.SAME, (d2m)Expression.And, "op_BitwiseAnd")); // BitAnd,   //x&y		30.And
            dic.Add("^", new Operator(OperatorType.BINARY, typeof(object), 72, null, null, RequiredOperandType.SAME, (d2m)Expression.ExclusiveOr, "op_ExclusiveOr")); // BitExclusiveOr,    //x^y		80.ExclusiveOr(XOR)
            dic.Add("|", new Operator(OperatorType.BINARY, typeof(object), 74, null, null, RequiredOperandType.SAME, (d2m)Expression.Or, "op_BitwiseOr")); // BitOr, //x|y		90.Or
            dic.Add("&&", new Operator(OperatorType.BINARY, typeof(bool), 80, null, null, RequiredOperandType.NONE, (d2m)Expression.AndAlso, "op_LogicalAnd")); // And,   //x&&y	100.AndAlso
            dic.Add("||", new Operator(OperatorType.BINARY, typeof(bool), 85, null, null, RequiredOperandType.NONE, (d2m)Expression.OrElse, "op_LogicalOr")); // Or,    //x||y	110.OrElse
            dic.Add("??", new Operator(OperatorType.BINARY, typeof(object), 90, null, null, RequiredOperandType.NONE, (d2)Expression.Coalesce, null)); // Coalesce,  //x??y	120.Coalesce
            //[Conditional]
            dic.Add("?", new Operator(OperatorType.CONDITIONAL, typeof(bool), 90)); // Condition, //  c?x:y	130.Condition
            dic.Add(":", new Operator(OperatorType.BINARY, typeof(object), 90)); // ConditionElse, // c?x:y	130.Condition
            //[Assignment]
            dic.Add("+=", new Operator(OperatorType.ASSIGN, typeof(double), 100, null, null, RequiredOperandType.SAME, (d2m)Expression.AddAssign, "op_AdditionAssignment")); // AddAssign, //x+=y	140.AddAssign
            dic.Add("-=", new Operator(OperatorType.ASSIGN, typeof(double), 100, null, null, RequiredOperandType.SAME, (d2m)Expression.SubtractAssign, "op_SubtractionAssignment")); // SubtractAssign,    //x-=y	140.SubtractAssign
            dic.Add("&=", new Operator(OperatorType.ASSIGN, typeof(bool), 100, null, null, RequiredOperandType.SAME, (d2m)Expression.AddAssign, "op_BitwiseAndAssignment")); // BitAndAssign, //x&=y	140.AndAssign
            dic.Add("=", new Operator(OperatorType.ASSIGN, typeof(object), 100, null, null, RequiredOperandType.SAME, (d2)Expression.Assign, "op_Assign")); // Assign,    //x=y		140.Assign
            dic.Add("/=", new Operator(OperatorType.ASSIGN, typeof(double), 100, null, null, RequiredOperandType.SAME, (d2m)Expression.DivideAssign, "op_DivisionAssignment")); // DivideAssign,  //x/=y	140.DivideAssign
            dic.Add("^=", new Operator(OperatorType.ASSIGN, typeof(bool), 100, null, null, RequiredOperandType.SAME, (d2m)Expression.ExclusiveOrAssign, "op_ExclusiveOrAssignment")); // BitExclusiveOrAssign,  //x^=y	140.ExclusiveOrAssign
            dic.Add("<<=", new Operator(OperatorType.ASSIGN, typeof(double), 100, null, typeof(int), RequiredOperandType.NONE, (d2m)Expression.LeftShiftAssign, "op_LeftShiftAssignment")); // LeftShiftAssign,   //x<<=y	140.LeftShiftAssign
            dic.Add(">>=", new Operator(OperatorType.ASSIGN, typeof(double), 100, null, typeof(int), RequiredOperandType.NONE, (d2m)Expression.RightShiftAssign, "op_RightShiftAssignment")); // RightShiftAssign,  //x>>=y	140.RightShiftAssign
            dic.Add("%=", new Operator(OperatorType.ASSIGN, typeof(double), 100, null, null, RequiredOperandType.SAME, (d2m)Expression.ModuloAssign, "op_ModulusAssignment")); // ModuloAssign,  //x%=y	140.ModuloAssign
            dic.Add("*=", new Operator(OperatorType.ASSIGN, typeof(double), 100, null, null, RequiredOperandType.SAME, (d2m)Expression.MultiplyAssign, "op_MultiplicationAssignment")); // MultiplyAssign,    //x*=y	140.MultiplyAssign
            dic.Add("**=", new Operator(OperatorType.ASSIGN, typeof(double), 100, null, null, RequiredOperandType.SAME, (d2m)Expression.PowerAssign, null)); // PowserAssign,    //x**=y	140.PowerAssign
            dic.Add("|=", new Operator(OperatorType.ASSIGN, typeof(bool), 100, null, null, RequiredOperandType.SAME, (d2m)Expression.OrAssign, "op_BitwiseOrAssignment")); // BitOrAssign   //x|=y	140.OrAssign
            return dic;
        }
    }
}