﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;

namespace Sigma.LendFoundry.Rules.Core.Mapping
{
    /// <summary>
    /// Type configuration base class which will be implemented by all mapping classes instead of directly implementing EntityTypeConfiguration.
    /// This allows us to perform additional configuration work/provide overridable methods here which will not be possible if mapping classes 
    /// directly extended EntityTypeConfiguration.
    /// </summary>
    public abstract class LfEntityTypeConfig<T> : EntityTypeConfiguration<T> where T : class
    {
    }
}
