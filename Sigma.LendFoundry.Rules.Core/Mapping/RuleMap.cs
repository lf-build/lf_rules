﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sigma.LendFoundry.Rules.Core.Models;

namespace Sigma.LendFoundry.Rules.Core.Mapping
{
    /// <summary>
    /// Mapper/configuration class for the Rule entity
    /// </summary>
    public class RuleMap : LfEntityTypeConfig<Rule>
    {
        public RuleMap()
        {
            this.ToTable("Rule");
            this.HasKey(s => s.Id);
        }
    }
}
