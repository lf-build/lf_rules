﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sigma.LendFoundry.Rules.Core.Models
{
    /// <summary>
    /// This class represents a variable that forms part of an expression assigned to a rule
    /// </summary>
    public class Variable
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string FriendlyName { get; set; }
        public string Description { get; set; }
        public string MainGroup { get; set; }
        
        // Need to figure out how to handle this
        // public string DataType { get; set; }
    }
}
