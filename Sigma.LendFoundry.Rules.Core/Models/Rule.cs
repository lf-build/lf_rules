﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sigma.LendFoundry.Rules.Core.Models
{
    /// <summary>
    /// This class represents a rule.
    /// </summary>
    public class Rule
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Expression { get; set; }
        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }
    }
}
