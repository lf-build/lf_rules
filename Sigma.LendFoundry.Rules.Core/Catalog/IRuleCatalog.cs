﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sigma.LendFoundry.Rules.Core.Models;

namespace Sigma.LendFoundry.Rules.Core.Catalog
{
    /// <summary>
    /// Rule catalog interface which exposes methods for rule CRUD operations. This is called from the API layer.
    /// </summary>
    public interface IRuleCatalog
    {
        void Create(Rule rule);
        void Update(Rule rule);
        void Delete(int id);
        Rule Get(int id);
        IEnumerable<Rule> GetAll();
    }
}
