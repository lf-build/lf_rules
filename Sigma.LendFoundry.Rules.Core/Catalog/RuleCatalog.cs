﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sigma.LendFoundry.Rules.Core.Models;
using Sigma.LendFoundry.Rules.Core.Repository;

namespace Sigma.LendFoundry.Rules.Core.Catalog
{
    /// <summary>
    /// Business layer object for CRUD operations for rules.
    /// </summary>
    public class RuleCatalog : IRuleCatalog
    {
        private readonly IRepository<Rule> _ruleRepository;

        /// <summary>
        /// Ctor
        /// </summary>
        public RuleCatalog(IRepository<Rule> ruleRepository)
        {
            _ruleRepository = ruleRepository;
        }

        /// <summary>
        /// Creates a new rule by invoking the repository
        /// </summary>
        /// <param name="rule"></param>
        public void Create(Rule rule)
        {
            _ruleRepository.Insert(rule);
        }

        /// <summary>
        /// Updates an existing rule by invoking the repository
        /// </summary>
        public void Update(Rule rule)
        {
            _ruleRepository.Update(rule);
        }

        /// <summary>
        /// Invokes the rule repository to delete a rule
        /// </summary>
        public void Delete(int id)
        {
            _ruleRepository.Delete(new Rule() { Id = id });
        }

        /// <summary>
        /// Gets a rule object for a given rule Id
        /// </summary>
        public Rule Get(int id)
        {
            Rule rule = _ruleRepository.GetById(id);
            return rule;
        }

        /// <summary>
        /// Gets all rules in the rules catalog
        /// </summary>
        public IEnumerable<Rule> GetAll()
        {
            var rules = _ruleRepository.TableNoTracking;
            return rules;
        }
    }
}
