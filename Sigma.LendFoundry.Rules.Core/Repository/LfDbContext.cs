﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sigma.LendFoundry.Rules.Core.Mapping;

namespace Sigma.LendFoundry.Rules.Core.Repository
{
    public class LfDbContext : DbContext, IDbContext
    {
        /// <summary>
        /// Ctor
        /// </summary>
        public LfDbContext(string nameOrConnectionString) : base(nameOrConnectionString)
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // right now this is being added manually here, but we can loop through the
            // executing assembly's type definitions and figure out a way to identify 
            // mapping classes and add them.
            modelBuilder.Configurations.Add(new RuleMap());
            base.OnModelCreating(modelBuilder);
        }

        public new IDbSet<TEntity> Set<TEntity>() where TEntity : class
        {
            return base.Set<TEntity>();
        }

        public IList<TEntity> ExecuteStoredProcedureList<TEntity>(string commandText, params object[] parameters) where TEntity : class, new()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<TElement> SqlQuery<TElement>(string sql, params object[] parameters)
        {
            throw new NotImplementedException();
        }

        public int ExecuteSqlCommand(string sql, bool doNotEnsureTransaction = false, int? timeout = null, params object[] parameters)
        {
            throw new NotImplementedException();
        }
    }
}
