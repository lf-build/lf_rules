﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sigma.LendFoundry.Rules.Core.Models;

namespace Sigma.LendFoundry.Rules.Core.Repository
{
    public interface IRuleRepository
    {
        int Create(Rule rule);
        void Update(Rule rule);
        void Delete(int id);
        Rule Get(int id);
        IEnumerable<Rule> GetAll();
    }
}
