﻿using System;
using System.Collections.ObjectModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sigma.LendFoundry.Rules.Api.Controllers;
using Sigma.LendFoundry.Rules.Core.Catalog;
using Sigma.LendFoundry.Rules.Core.Models;
using Sigma.LendFoundry.Rules.Core.Repository;
using Moq;
using System.Web.Http;
using Sigma.LendFoundry.Rules.Api;
using System.Threading;

namespace Sigma.LendFoundry.Rules.Tests
{
    [TestClass]
    public class RulesControllerTests
    {
        [TestInitialize]
        public void Initialize()
        {
            try
            {
                RunAppInitializationCode();
            }
            catch(InvalidOperationException ex)
            {
                Thread.Sleep(2000);
                RunAppInitializationCode();
            }
        }

        private void RunAppInitializationCode()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            GlobalConfiguration.Configure(AutofacConfig.Register);
        }


        [TestMethod]
        public void CreateRule()
        {
            var mock = new Mock<IDbContext>();
            IDbContext context = mock.Object;
            EfRepository<Rule> repository = new EfRepository<Rule>(context);
            RuleCatalog catalog = new RuleCatalog(repository);
            CatalogController controller = new CatalogController(catalog);
            Rule rule = new Rule();
            var result = controller.CreateRule(rule);

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetRuleDetails()
        {
            var mock = new Mock<IDbContext>();
            IDbContext context = mock.Object;
            EfRepository<Rule> repository = new EfRepository<Rule>(context);
            RuleCatalog catalog = new RuleCatalog(repository);
            CatalogController controller = new CatalogController(catalog);
            int ruleId = 1;
            var rule = controller.GetRuleDetails(ruleId);

            Assert.IsNotNull(rule);
        }

        [TestMethod]
        public void GetAllRules()
        {
            var mock = new Mock<IDbContext>();
            IDbContext context = mock.Object;
            EfRepository<Rule> repository = new EfRepository<Rule>(context);
            RuleCatalog catalog = new RuleCatalog(repository);
            CatalogController controller = new CatalogController(catalog);
            var rules = controller.GetAllRules();

            Assert.IsNotNull(rules);
        }

        [TestMethod]
        public void UpdateRule()
        {
            var mock = new Mock<IDbContext>();
            IDbContext context = mock.Object;
            EfRepository<Rule> repository = new EfRepository<Rule>(context);
            RuleCatalog catalog = new RuleCatalog(repository);
            CatalogController controller = new CatalogController(catalog);

            Rule rule = new Rule();
            rule.Id = 1;
            rule.Name = "UpdatedRuleName";
            rule.Expression = "";

            var result = controller.UpdateRule(rule.Id,rule);
        }

        [TestMethod]
        public void DeleteRule()
        {
            
        }

        [TestMethod]
        public void EvaluateRule()
        {
            // TODO : Implement test
        }
    }
}