﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Sigma.LendFoundry.Rules.Api
{
    public static class WebApiConfig
    {
        private static bool _isRegistered;

        public static void Register(HttpConfiguration config)
        {
            if (_isRegistered)
                return;

            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "v1/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            _isRegistered = true;
        }
    }
}
