﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Compilation;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using Sigma.LendFoundry.Rules.Core.Catalog;
using Sigma.LendFoundry.Rules.Core.Repository;

namespace Sigma.LendFoundry.Rules.Api
{
    public class AutofacConfig
    {
        private static IContainer Container { get; set; }

        public static void Register(HttpConfiguration config)
        {
            var builder = new ContainerBuilder();
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            // register broker/service layer types
            builder.RegisterType<RuleCatalog>().As<IRuleCatalog>();

            builder.Register<IDbContext>(c => new LfDbContext(connectionString));
            
            // register repository types
            builder.RegisterGeneric(typeof(EfRepository<>)).As(typeof(IRepository<>)).InstancePerLifetimeScope();

            // register api controllers
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());


            Container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(Container);
        }
    }
}