﻿using Ninject;
using Phoenix.Common;
using Phoenix.Repository;
using Phoenix.Repository.Model;
using Phoenix.Services.RuleEngine;
using Phoenix.WebApplication.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Phoenix.WebApplication.Controllers
{
    /// <summary>
    /// Controller for Rules API
    /// </summary>
    public class RuleApiController : ApiController
    {
        private readonly IRulePreScreenRepository _rulePreScreenRepository;
        private readonly IRuleRepository _ruleRepository;
        private readonly IUnitOfWork _unitofwork;

        /// <summary>
        /// Constructor for Rule API Controller
        /// </summary>
        /// <param name="UnitofWork">             </param>
        /// <param name="ruleRepository">         </param>
        /// <param name="rulePreScreenRepository"></param>
        public RuleApiController(IUnitOfWork UnitofWork, IRuleRepository ruleRepository, IRulePreScreenRepository rulePreScreenRepository)
        {
            _ruleRepository = ruleRepository;
            _rulePreScreenRepository = rulePreScreenRepository;
            _unitofwork = UnitofWork;
        }

        [Inject]
        public IAlertFactory _alertFactory { get; set; }

        [Inject]
        public IBrooklynConfigurationRepository _brooklynConfigurationRepository { get; set; }

        [Inject]
        public IExpressionEvaluator _expressionEvaluator { get; set; }

        [Inject]
        public ILineAssignmentRepository _lineAssignmentRepository { get; set; }

        [Inject]
        public IRuleFactory _ruleFactory { get; set; }

        #region Get Rule List

        /// <summary>
        /// Delete Rule from Prescreen
        /// </summary>
        /// <param name="Entity"></param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        public HttpResponseMessage DeletePreScreen(int PreScreenId)
        {
            var currentList = _rulePreScreenRepository.All().Where(items => items.DealPreScreenEnumId == PreScreenId);
            foreach (var i in currentList)
            {
                _rulePreScreenRepository.Delete(i);
            };

            _unitofwork.SaveChanges();
            return Request.CreateResponse(HttpStatusCode.OK, true);
        }

        /// <summary>
        /// Evaluate the Expression
        /// </summary>
        /// <param name="strExpression">Expression Paramenter in String</param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        public HttpResponseMessage EvaluateExpression(dynamic strExpression)
        {
            try
            {
                _expressionEvaluator.TestEvaluation(strExpression);
                return Request.CreateResponse(HttpStatusCode.OK, "Success");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }
        }

        /// <summary>
        /// Get Prescreen List with Rules
        /// </summary>
        /// <returns></returns>
        public HttpResponseMessage GetPreScreenList()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _ruleFactory.GetPreScreenList());
        }

        /// <summary>
        /// Get Rule List
        /// </summary>
        /// <returns>List of Rule</returns>
        public HttpResponseMessage GetRuleList()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _ruleRepository.All());
        }

        /// <summary>
        /// Get Rule List
        /// </summary>
        /// <returns>List of Rule</returns>
        public HttpResponseMessage GetRuleList(int Page, int PageNo, string SortField, bool SortDirection)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _ruleFactory.GetRuleList(Page, PageNo, SortField, SortDirection));
        }

        /// <summary>
        /// Add/Edit Rule Details
        /// </summary>
        /// <param name="rules"></param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        public HttpResponseMessage SaveRuleDetail(Rule rules)
        {
            try
            {
                //Evaluate the Expression
                // Phoenix.Services.RuleEngine.ExpressionEvaluator e = new ExpressionEvaluator();
                _expressionEvaluator.TestEvaluation(rules.RuleExpression);

                var curUser = StateManagement.GetUserSession();
                rules.UpdatedBy = curUser.UserId;
                rules.UpdatedOn = DateTime.Now;
                if (rules.RuleId > 0)
                {
                    _ruleRepository.Update(rules);
                }
                else
                {
                    rules.CreatedBy = curUser.UserId;
                    rules.CreatedOn = DateTime.Now;
                    _ruleRepository.Insert(rules);
                }
                _unitofwork.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, _ruleRepository.All());
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Associate Rule Id with Prescreen Option
        /// </summary>
        /// <param name="Entity"></param>
        /// <param name="RuleId"></param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        public HttpResponseMessage UpdateBrooklynConfiguration(int BrooklynConfigurationId, int? RuleId)
        {
            var rules = _brooklynConfigurationRepository.All().FirstOrDefault(items => items.BrooklynConfigurationID == BrooklynConfigurationId);

            if (rules != null)
            {
                rules.RuleId = RuleId;
                rules.UpdatedBy = StateManagement.GetUserSession().UserId;
                rules.UpdatedOn = DateTime.Now;
                _brooklynConfigurationRepository.Update(rules);
            }
            //else
            //{
            //    RulePreScreen rp = new RulePreScreen();
            //    rp.RuleId = RuleId;
            //    rp.DealPreScreenEnumId = PrescreenId;
            //    rp.CreatedBy = StateManagement.GetUserSession().UserId;
            //    rp.CreatedOn = DateTime.Now;
            //    rp.UpdatedBy = StateManagement.GetUserSession().UserId;
            //    rp.UpdatedOn = DateTime.Now;
            //    _rulePreScreenRepository.Insert(rp);
            //}
            _unitofwork.SaveChanges();
            return Request.CreateResponse(HttpStatusCode.OK, true);
        }

        /// <summary>
        /// Associate Rule Id with Prescreen Option
        /// </summary>
        /// <param name="Entity"></param>
        /// <param name="RuleId"></param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        public HttpResponseMessage UpdatePreScreen(int PrescreenId, int RuleId)
        {
            var rules = _rulePreScreenRepository.All().Where(items => items.DealPreScreenEnumId == PrescreenId);

            var curUser = StateManagement.GetUserSession();
            if (rules != null && rules.ToList().Count > 0)
            {
                foreach (var i in rules)
                {
                    i.RuleId = RuleId;
                    i.UpdatedBy = curUser.UserId;
                    i.UpdatedOn = DateTime.Now;
                    _rulePreScreenRepository.Update(i);
                };
            }
            else
            {
                RulePreScreen rp = new RulePreScreen();
                rp.RuleId = RuleId;
                rp.DealPreScreenEnumId = PrescreenId;
                rp.CreatedBy = curUser.UserId;
                rp.CreatedOn = DateTime.Now;
                rp.UpdatedBy = curUser.UserId;
                rp.UpdatedOn = DateTime.Now;
                _rulePreScreenRepository.Insert(rp);
            }
            _unitofwork.SaveChanges();
            return Request.CreateResponse(HttpStatusCode.OK, true);
        }

        #endregion Get Rule List

        /// <summary>
        /// Get Current Alert List
        /// </summary>
        /// <returns>List of Rule</returns>
        public HttpResponseMessage GetBrooklynConfiguration()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _ruleFactory.GetBrooklynConfiguration());
        }

        /// <summary>
        /// Get Current Alert List
        /// </summary>
        /// <returns>List of Rule</returns>
        public HttpResponseMessage GetBrooklynScore(int DealId)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _ruleFactory.GetBrooklynScore(DealId));
        }

        /// <summary>
        /// Get Current Alert List
        /// </summary>
        /// <returns>List of Rule</returns>
        public HttpResponseMessage GetCurrentAlertList(int DealId)
        {
            LoggerFactory.LoggerInstance.LogInformation("For"+DealId+"Get Alert started at :"+DateTime.Now);
            return Request.CreateResponse(HttpStatusCode.OK, _alertFactory.ApplyAlerts(DealId));
           
        }

        /// <summary>
        /// Get Current Alert List
        /// </summary>
        /// <returns>List of Rule</returns>
        public HttpResponseMessage GetLineAssignment()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _lineAssignmentRepository.All().ToList().OrderBy(c => c.PriceType).ThenBy(items => items.FromScore));
        }

        /// <summary>
        /// Save Data Maintenance
        /// </summary>
        /// <returns></returns>
        public HttpResponseMessage SaveDataMaintenance(List<LineAssignment> lineAssignmentList)
        {
            if (lineAssignmentList != null)
            {
                var currentList = _lineAssignmentRepository.All();

                foreach (var entity in lineAssignmentList)
                {
                    var newEntity = currentList.FirstOrDefault(items => items.ScoreCardID == entity.ScoreCardID);
                    newEntity.DurationFactor = entity.DurationFactor;
                    newEntity.FromScore = entity.FromScore;
                    newEntity.MaxAmount = entity.MaxAmount;
                    newEntity.MaxTermMos = entity.MaxTermMos;
                    newEntity.MinAmount = entity.MinAmount;
                    newEntity.MinTermMos = entity.MinTermMos;
                    newEntity.MultiplierV1 = entity.MultiplierV1;
                    newEntity.MultiplierV2 = entity.MultiplierV2;
                    newEntity.MultiplierV3 = entity.MultiplierV3;
                    newEntity.PriceFactor = entity.PriceFactor;
                    newEntity.ToScore = entity.ToScore;

                    _lineAssignmentRepository.Update(newEntity);
                }
                _unitofwork.SaveChanges();
            }

            return Request.CreateResponse(HttpStatusCode.OK, true);
        }
    }
}