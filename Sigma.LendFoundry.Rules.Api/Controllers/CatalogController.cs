﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Sigma.LendFoundry.Rules.Api.Filters;
using Sigma.LendFoundry.Rules.Core.Catalog;
using Sigma.LendFoundry.Rules.Core.Models;

namespace Sigma.LendFoundry.Rules.Api.Controllers
{
    /// <summary>
    /// Web API controller class for managing CRUD operations for rules
    /// </summary>
    [NotImplExceptionFilter]
    [RoutePrefix("v1/catalog")]
    public class CatalogController : ApiController
    {

        #region Properties
        /// <summary>
        /// Business logic layer catalog object.
        /// </summary>
        public IRuleCatalog Catalog { get; set; }
        #endregion Properties

        #region Ctor
        /// <summary>
        /// Ctor
        /// </summary>
        public CatalogController(IRuleCatalog catalog)
        {
            Catalog = catalog;
        }
        #endregion Ctor

        #region Rule APIs
        /// <summary>
        /// Create a rule
        /// </summary>
        [HttpPost]
        [Route("rules")]
        public IHttpActionResult CreateRule([FromBody]Rule rule)
        {
            Catalog.Create(rule);
            return Ok();
        }

        /// <summary>
        /// Get information about a rule
        /// </summary>
        [HttpGet]
        [Route("rules/{id}")]
        public IHttpActionResult GetRuleDetails(int id)
        {
            var response = Catalog.Get(id);
            return Ok(response);
        }

        /// <summary>
        /// Get list of all rules
        /// </summary>
        [HttpGet]
        [Route("rules")]
        public IHttpActionResult GetAllRules()
        {
            var response = Catalog.GetAll();
            return Ok(response);
        }

        /// <summary>
        /// Delete a rule
        /// </summary>
        [HttpDelete]
        [Route("rules/{id}")]
        public IHttpActionResult DeleteRule(int id)
        {
            Catalog.Delete(id);
            return Ok();
        }

        /// <summary>
        /// Update a rule
        /// </summary>
        [HttpPut, HttpPost] // Using post instead of put here because some clients may not support put. http://stackoverflow.com/questions/25460427/asp-net-mvc-and-web-api-which-is-better-http-post-or-put
        [Route("rules/{id}")]
        public IHttpActionResult UpdateRule([FromUri] int id, [FromBody]Rule rule)
        {
            Catalog.Update(rule);
            return Ok();
        }
        #endregion Rule APIs

        #region Rule Variable APIs
        //----------------------------------------------------------------------------------------------
        // CRUD operations for rule variables - Would these really be required?
        //----------------------------------------------------------------------------------------------
        [HttpPost]
        [Route("variables")]
        public IHttpActionResult CreateVariable([FromBody] Variable variable)
        {
            throw new NotImplementedException();
        }

        [HttpPost]
        [Route("v1/catalog/UpdateVariable")]
        public IHttpActionResult UpdateVariable([FromBody] Variable variable)
        {
            throw new NotImplementedException();
        }

        [HttpGet]
        [Route("v1/catalog/GetVariableDetails")]
        public IHttpActionResult GetVariableDetails(int Id)
        {
            throw new NotImplementedException();
        }

        [HttpGet]
        [Route("v1/catalog/GetAllVariables")]
        public IHttpActionResult GetAllVariables()
        {
            throw new NotImplementedException();
        }

        // TODO : SHOULD WE ALLOW DELETE VARIABLE?

        #endregion Rule Variable APIs

        #region Function, Expression and Operator APIs
        //----------------------------------------------------------------------------------------------
        // Operations for getting function, expression fields, rules and operators - Should we merely get the 
        // functions or should we also allow Create, Update, Delete operations?
        //----------------------------------------------------------------------------------------------
        [HttpGet]
        [Route("v1/catalog/GetFunctionList")]
        public IHttpActionResult GetAllFunctions()
        {
            throw new NotImplementedException();
        }

        [HttpGet]
        public IHttpActionResult GetAllExpressions()
        {
            throw new NotImplementedException();
        }


        public IHttpActionResult GetAllOperators()
        {
            throw new NotImplementedException();
        }
        #endregion Function, Expression and Operator APIs

        #region Prescreen Rule APIs
        //----------------------------------------------------------------------------------------------
        // Operations to map/unmap rules applicable to prescreen
        //----------------------------------------------------------------------------------------------
        [HttpGet]
        [Route("v1/catalog/GetAllPrescreenRules")]
        public IHttpActionResult GetAllPrescreenRules()
        {
            throw new NotImplementedException();
        }

        [HttpPost]
        [Route("v1/catalog/UpdatePrescreen")]
        public IHttpActionResult UpdatePrescreen()
        {
            throw new NotImplementedException();
        }

        #endregion Prescreen Rule APIs


        //----------------------------------------------------------------------------------------------
        // CRUD operations for decision table data maintenance
        //----------------------------------------------------------------------------------------------

        //----------------------------------------------------------------------------------------------
        // CRUD operations for alerts
        //----------------------------------------------------------------------------------------------

        //----------------------------------------------------------------------------------------------
        // Need to figure out if the "Waterfall" link in phoenix which contains list of rules is 
        // applicable here
        //----------------------------------------------------------------------------------------------
    }
}
