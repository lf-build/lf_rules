﻿using Phoenix.Repository;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Phoenix.WebApplication.Controllers
{
    /// <summary>
    /// Controller for Variable List
    /// </summary>
    public class VariableListApiController : ApiController
    {
        private readonly IUnitOfWork _unitofwork;
        private readonly IVariableListRepository _variableListRepository;

        private readonly IVariableVersionRepository _variableVersionRepository;

        /// <summary>
        /// Construction for Variable List
        /// </summary>
        /// <param name="UnitofWork">               </param>
        /// <param name="variableListRepository">   </param>
        /// <param name="variableVersionRepository"></param>
        public VariableListApiController(IUnitOfWork UnitofWork, IVariableListRepository variableListRepository, IVariableVersionRepository variableVersionRepository)
        {
            _variableListRepository = variableListRepository;
            _variableVersionRepository = variableVersionRepository;
            _unitofwork = UnitofWork;
        }

        #region Get Variable List

        public HttpResponseMessage GetCurrentVariableList()
        {
            var x = (from p in _variableVersionRepository.All()
                     //where conditions or joins with other tables to be included here
                     group p by p.VariableID into grp
                     let MaxVersionNumber = grp.Max(g => g.VersionNumber)
                     from val in grp
                     where val.VersionNumber == MaxVersionNumber
                     select val);
            return Request.CreateResponse(HttpStatusCode.OK, true);
        }

        /// <summary>
        /// This function returns the list of Variable
        /// </summary>
        /// <returns></returns>
        public HttpResponseMessage GetVariableList()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _variableListRepository.All());
        }

        #endregion Get Variable List
    }
}