﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sigma.LendFoundry.Rules.Client.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View("ClientHome");
        }

        public ActionResult ClientHome()
        {
            return View("ClientHome");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}