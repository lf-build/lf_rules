﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sigma.LendFoundry.Rules.Client.Controllers
{
    public class RuleController : Controller
    {
        //
        // GET: /Rule/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CreateRule()
        {
            return View();
        }


        public ActionResult GetAllRules()
        {
            return View("RuleList");
        }
	}
}