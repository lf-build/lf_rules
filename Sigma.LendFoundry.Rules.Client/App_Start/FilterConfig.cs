﻿using System.Web;
using System.Web.Mvc;

namespace Sigma.LendFoundry.Rules.Client
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
